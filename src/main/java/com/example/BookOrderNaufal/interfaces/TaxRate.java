package com.example.BookOrderNaufal.interfaces;

import java.math.BigDecimal;

public interface TaxRate {
	final BigDecimal taxRate = new BigDecimal(0.05);
}
