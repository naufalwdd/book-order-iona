package com.example.BookOrderNaufal.interfaces;

import java.math.BigDecimal;

public interface DiscountRate {
	final BigDecimal discountRate = new BigDecimal(0.1);
}
