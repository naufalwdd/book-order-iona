package com.example.BookOrderNaufal.interfaces;

import java.math.BigDecimal;

public interface BookPriceRate {
	final BigDecimal bookPriceRate = new BigDecimal(1.5);
}
