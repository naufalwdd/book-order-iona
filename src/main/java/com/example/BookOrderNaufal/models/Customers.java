package com.example.BookOrderNaufal.models;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "customers")
public class Customers implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7569939487834712055L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_customer_id_customer_seq")
	@SequenceGenerator(name = "generator_customer_id_customer_seq", sequenceName = "customer_id_customer_seq", schema = "public", allocationSize = 1)
	@Column(name = "customer_id", nullable = false)
	private Long customerId;
	
	@Column(name = "customer_name", nullable = false)
	private String customerName;
	
	@Column(name = "country", nullable = false)
	private String country;
	
	@Column(name = "address", nullable = false)
	private String address;
	
	@Column(name = "phone_number", nullable = false)
	private String phoneNumber;
	
	@Column(name = "postal_code", nullable = false)
	private String postalCode;
	
	@Column(name = "email", nullable = false)
	private String email;
	
	@OneToMany(mappedBy = "customers")
	private Set<Order> order;
	
	// Constructor
	public Customers() {
		// TODO Auto-generated constructor stub
	}

	public Customers(Long customerId, String customerName, String country, String address, String phoneNumber,
			String postalCode, String email, Set<Order> order) {
		super();
		this.customerId = customerId;
		this.customerName = customerName;
		this.country = country;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.postalCode = postalCode;
		this.email = email;
		this.order = order;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<Order> getOrder() {
		return order;
	}

	public void setOrder(Set<Order> order) {
		this.order = order;
	}
}
