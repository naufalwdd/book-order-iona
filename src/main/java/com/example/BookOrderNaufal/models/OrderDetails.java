package com.example.BookOrderNaufal.models;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "order_details")
public class OrderDetails implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5310838689715891397L;

	@EmbeddedId
	private OrderDetailsKey orderKey;
	
	@Column(name = "quantity", nullable = false)
	private int quantity;
	
	@Column(name = "discount", nullable = false)
	private BigDecimal discount;
	
	@Column(name = "tax", nullable = false)
	private BigDecimal tax;
	
	@ManyToOne
	@MapsId("order_id")
	@JoinColumn(name = "order_id")
	private Order order;
	
	@ManyToOne
	@MapsId("book_id")
	@JoinColumn(name = "book_id")
	private Book book;
	
	// Constructor
	public OrderDetails() {
		// TODO Auto-generated constructor stub
	}

	public OrderDetails(OrderDetailsKey orderKey, int quantity, BigDecimal discount, BigDecimal tax, Order order, Book book) {
		super();
		this.orderKey = orderKey;
		this.quantity = quantity;
		this.discount = discount;
		this.tax = tax;
		this.order = order;
		this.book = book;
	}

	// Getter Setter
	public OrderDetailsKey getOrderKey() {
		return orderKey;
	}

	public void setOrderKey(OrderDetailsKey orderKey) {
		this.orderKey = orderKey;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public BigDecimal getTax() {
		return tax;
	}

	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}
}
