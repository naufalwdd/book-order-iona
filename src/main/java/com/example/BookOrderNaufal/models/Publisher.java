package com.example.BookOrderNaufal.models;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "publisher")
public class Publisher implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7912205155386003351L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_publisher_id_publisher_seq")
	@SequenceGenerator(name = "generator_publisher_id_publisher_seq", sequenceName = "publisher_id_publisher_seq", schema = "public", allocationSize = 1)
	@Column(name = "publisher_id", nullable = false)
	private Long publisherId;
	
	@Column(name = "company_name", nullable = false)
	private String companyName;
	
	@Column(name = "country")
	private String country;
	
	@ManyToOne
	@JoinColumn(name = "paper_id")
	private Paper paper;

	@OneToMany(mappedBy = "publisher")
	private Set<Book> book;
	
	// Constructor
	public Publisher() {
		// TODO Auto-generated constructor stub
	}
	public Publisher(Long publisherId, String companyName, String country, Paper paper, Set<Book> book) {
		super();
		this.publisherId = publisherId;
		this.companyName = companyName;
		this.country = country;
		this.paper = paper;
		this.book = book;
	}


	// Getter Setter
	public Long getPublisherId() {
		return publisherId;
	}
	public void setPublisherId(Long publisherId) {
		this.publisherId = publisherId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Paper getPaper() {
		return paper;
	}
	public void setPaper(Paper paper) {
		this.paper = paper;
	}
}
