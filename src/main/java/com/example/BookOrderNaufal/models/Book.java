package com.example.BookOrderNaufal.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "book")
public class Book implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_book_id_book_seq")
	@SequenceGenerator(name = "generator_book_id_book_seq", sequenceName = "book_id_book_seq", schema = "public", allocationSize = 1)
	@Column(name = "book_id", nullable = false)
	private Long bookId;
	
	@Column(name = "title", nullable = false)
	private String title;
	
	@Column(name = "release_date", nullable = false)
	private Date releaseDate;
	
	@Column(name = "price", nullable = false)
	private BigDecimal price;
	
	@ManyToOne
	@JoinColumn(name = "publisher_id")
	private Publisher publisher;
	
	@ManyToOne
	@JoinColumn(name = "author_id")
	private Author author;
	
	@OneToMany(mappedBy = "book")
	private Set<BookRatings> bookRatings;
	
	@OneToMany(mappedBy = "book")
	private Set<OrderDetails> orderDetails;
	
	// Constructor
	public Book() {
		// TODO Auto-generated constructor stub
	}

	public Book(Long bookId, String title, Date releaseDate, BigDecimal price, Publisher publisher, Author author,
			Set<BookRatings> bookRatings, Set<OrderDetails> orderDetails) {
		super();
		this.bookId = bookId;
		this.title = title;
		this.releaseDate = releaseDate;
		this.price = price;
		this.publisher = publisher;
		this.author = author;
		this.bookRatings = bookRatings;
		this.orderDetails = orderDetails;
	}

	public Long getBookId() {
		return bookId;
	}

	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Publisher getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public Set<BookRatings> getBookRatings() {
		return bookRatings;
	}

	public void setBookRatings(Set<BookRatings> bookRatings) {
		this.bookRatings = bookRatings;
	}

	public Set<OrderDetails> getOrderDetails() {
		return orderDetails;
	}

	public void setOrderDetails(Set<OrderDetails> orderDetails) {
		this.orderDetails = orderDetails;
	}
}
