package com.example.BookOrderNaufal.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "orders")
public class Order implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5482025464330476246L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_order_id_order_seq")
	@SequenceGenerator(name = "generator_order_id_order_seq", sequenceName = "order_id_order_seq", schema = "public", allocationSize = 1)
	@Column(name = "order_id", nullable = false)
	private Long orderId;
	
	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customers customers;
	
	@Column(name = "order_date", nullable = false)
	private Date orderDate;
	
	@Column(name = "total_order")
	private BigDecimal totalOrder;
	
	@OneToMany(mappedBy = "order")
	private Set<OrderDetails> orderDetails;
	
	// Constructor
	public Order() {
		// TODO Auto-generated constructor stub
	}

	public Order(Long orderId, Customers customers, Date orderDate, BigDecimal totalOrder, Order order, Set<OrderDetails> orderDetails) {
		super();
		this.orderId = orderId;
		this.customers = customers;
		this.orderDate = orderDate;
		this.totalOrder = totalOrder;
		this.orderDetails = orderDetails;
	}

	// Getter Setter
	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Customers getCustomers() {
		return customers;
	}

	public void setCustomers(Customers customers) {
		this.customers = customers;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public BigDecimal getTotalOrder() {
		return totalOrder;
	}

	public void setTotalOrder(BigDecimal totalOrder) {
		this.totalOrder = totalOrder;
	}

	public Set<OrderDetails> getOrderDetails() {
		return orderDetails;
	}

	public void setOrderDetails(Set<OrderDetails> orderDetails) {
		this.orderDetails = orderDetails;
	}
}
