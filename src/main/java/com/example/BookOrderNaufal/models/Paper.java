package com.example.BookOrderNaufal.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "paper")
public class Paper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -538635243256929708L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_paper_id_paper_seq")
	@SequenceGenerator(name = "generator_paper_id_paper_seq", sequenceName = "paper_id_paper_seq", schema = "public", allocationSize = 1)
	@Column(name = "paper_id", nullable = false)
	private Long paperId;
	
	@Column(name = "quality_name", nullable = false)
	private String qualityName;
	
	@Column(name = "paper_price", nullable = false)
	private BigDecimal paperPrice;
	
	@OneToMany(mappedBy = "paper")
	private Set<Publisher> publisher;

	// Constructor
	
	public Paper() {
		// TODO Auto-generated constructor stub
	}
	public Paper(Long paperId, String qualityName, BigDecimal paperPrice, Set<Publisher> publisher) {
		super();
		this.paperId = paperId;
		this.qualityName = qualityName;
		this.paperPrice = paperPrice;
		this.publisher = publisher;
	}
	
	// Getter Setter
	public Long getPaperId() {
		return paperId;
	}
	public void setPaperId(Long paperId) {
		this.paperId = paperId;
	}
	public String getQualityName() {
		return qualityName;
	}
	public void setQualityName(String qualityName) {
		this.qualityName = qualityName;
	}
	public BigDecimal getPaperPrice() {
		return paperPrice;
	}
	public void setPaperPrice(BigDecimal paperPrice) {
		this.paperPrice = paperPrice;
	}
	public Set<Publisher> getPublisher() {
		return publisher;
	}
	public void setPublisher(Set<Publisher> publisher) {
		this.publisher = publisher;
	}
}
