package com.example.BookOrderNaufal.models;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "reviewer")
public class Reviewer implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6529810258045694160L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_reviewer_id_reviewer_seq")
	@SequenceGenerator(name = "generator_reviewer_id_reviewer_seq", sequenceName = "reviewer_id_reviewer_seq", schema = "public", allocationSize = 1)
	@Column(name = "reviewer_id", nullable = false)
	private Long reviewerId;
	
	@Column(name = "reviewer_name")
	private String reviewerName;
	
	@Column(name = "country")
	private String country;
	
	@Column(name = "verified")
	private boolean verified;
	
	@OneToMany(mappedBy = "reviewer")
	private Set<BookRatings> bookRatings;
	
	// Constructor
	public Reviewer() {
		// TODO Auto-generated constructor stub
	}

	public Reviewer(Long reviewerId, String reviewerName, String country, boolean verified,
			Set<BookRatings> bookRatings) {
		super();
		this.reviewerId = reviewerId;
		this.reviewerName = reviewerName;
		this.country = country;
		this.verified = verified;
		this.bookRatings = bookRatings;
	}

	// Getter Setter
	public Long getReviewerId() {
		return reviewerId;
	}

	public void setReviewerId(Long reviewerId) {
		this.reviewerId = reviewerId;
	}

	public String getReviewerName() {
		return reviewerName;
	}

	public void setReviewerName(String reviewerName) {
		this.reviewerName = reviewerName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public boolean isVerified() {
		return verified;
	}

	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	public Set<BookRatings> getBookRatings() {
		return bookRatings;
	}

	public void setBookRatings(Set<BookRatings> bookRatings) {
		this.bookRatings = bookRatings;
	}
	
}
