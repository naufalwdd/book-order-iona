package com.example.BookOrderNaufal.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "book_ratings")
public class BookRatings implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9159499975786219226L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_rating_id_rating_seq")
	@SequenceGenerator(name = "generator_rating_id_rating_seq", sequenceName = "rating_id_rating_seq", schema = "public", allocationSize = 1)
	@Column(name = "book_rating_id", nullable = false)
	private Long bookRatingId;
	
	@ManyToOne
	@JoinColumn(name = "book_id")
	private Book book;
	
	@ManyToOne
	@JoinColumn(name = "reviewer_id")
	private Reviewer reviewer;
	
	@Column(name = "rating_score", nullable = false)
	private int ratingScore;
	
	// Constructor
	public BookRatings() {
		// TODO Auto-generated constructor stub
	}

	public BookRatings(Long bookRatingId, Book book, Reviewer reviewer, int ratingScore) {
		super();
		this.bookRatingId = bookRatingId;
		this.book = book;
		this.reviewer = reviewer;
		this.ratingScore = ratingScore;
	}

	// Getter Setter
	public Long getBookRatingId() {
		return bookRatingId;
	}

	public void setBookRatingId(Long bookRatingId) {
		this.bookRatingId = bookRatingId;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public Reviewer getReviewer() {
		return reviewer;
	}

	public void setReviewer(Reviewer reviewer) {
		this.reviewer = reviewer;
	}

	public int getRatingScore() {
		return ratingScore;
	}

	public void setRatingScore(int ratingScore) {
		this.ratingScore = ratingScore;
	}
}
