package com.example.BookOrderNaufal.controllersIona;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.dtos.PublisherDto;
import com.example.BookOrderNaufal.exception.ResourceNotFoundException;
import com.example.BookOrderNaufal.models.Publisher;
import com.example.BookOrderNaufal.repositories.PaperRepository;
import com.example.BookOrderNaufal.repositories.PublisherRepository;
import com.io.iona.springboot.actionflows.custom.CustomBeforeInsert;
import com.io.iona.springboot.controllers.HibernateCRUDController;
import com.io.iona.springboot.sources.HibernateDataSource;
import com.io.iona.springboot.sources.HibernateDataUtility;

@RestController
@RequestMapping("/publisher")
public class PublisherControllerCustom extends HibernateCRUDController<Publisher, PublisherDto> 
	implements CustomBeforeInsert<Publisher, PublisherDto> {

	@Autowired
	PublisherRepository publisherRepo;
	
	@Autowired
	PaperRepository paperRepo;
	
	ModelMapper modelMapper = new ModelMapper();
	
	@Override
	public void beforeInsert(HibernateDataUtility hibenateDataUtility, HibernateDataSource<Publisher, PublisherDto> dataSource) throws Exception {
		Publisher publisher = dataSource.getDataModel();
		PublisherDto publisherDto = modelMapper.map(publisher, PublisherDto.class);
		paperRepo.findById(publisherDto.getPaperId())
			.orElseThrow(() -> new ResourceNotFoundException("Paper", "paperId", publisherDto.getPaperId()));
	}
}
