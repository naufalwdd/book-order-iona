package com.example.BookOrderNaufal.controllersIona;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.dtos.BookDto;
import com.example.BookOrderNaufal.models.Author;
import com.example.BookOrderNaufal.models.Book;
import com.example.BookOrderNaufal.repositories.AuthorRepository;
import com.example.BookOrderNaufal.repositories.BookRepository;
import com.io.iona.springboot.controllers.HibernateCRUDController;
import com.io.iona.springboot.sources.HibernateDataSource;
import com.io.iona.springboot.sources.HibernateDataUtility;
import com.io.iona.springboot.actionflows.custom.CustomOnInsert;

@RestController
@RequestMapping("/book")
public class BookControllerCustom extends HibernateCRUDController<Book, BookDto> implements CustomOnInsert<Book, BookDto>{

	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	BookRepository bookRepo;
	
	@Autowired
	AuthorRepository authorRepo;
	
	@Override
	public Book onInsert(HibernateDataUtility hibernateDataUtility, HibernateDataSource<Book, BookDto> dataSource) throws Exception {
		Book book = dataSource.getDataModel();
		authorRepo.findById(book.getAuthor().getAuthorId());
		if (authorRepo.findById(book.getAuthor().getAuthorId()).isPresent() == false) {
			Author author = new Author();
			author.setAuthorId(book.getBookId());
			author.setAge(0);
			author.setCountry("Indonesia");
			author.setFirstName("New First Name");
			author.setLastName("New Last Name");
			author.setGender("M/L");
			author.setRating("New");
			authorRepo.save(author);
		}
		bookRepo.save(book);
		return dataSource.getDataModel();
	}
}
