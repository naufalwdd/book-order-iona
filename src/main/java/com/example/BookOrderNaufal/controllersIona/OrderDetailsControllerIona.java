package com.example.BookOrderNaufal.controllersIona;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.dtos.OrderDetailsDto;
import com.example.BookOrderNaufal.models.OrderDetails;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/api/iona/orderDetails")
public class OrderDetailsControllerIona extends HibernateCRUDController<OrderDetails, OrderDetailsDto> {

}
