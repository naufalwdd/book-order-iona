package com.example.BookOrderNaufal.controllersIona;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.dtos.OrderDto;
import com.example.BookOrderNaufal.models.Order;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/api/iona/order")
public class OrderControllerIona extends HibernateCRUDController<Order, OrderDto> {

}
