package com.example.BookOrderNaufal.controllersIona;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.dtos.PublisherDto;
import com.example.BookOrderNaufal.models.Publisher;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/api/iona/publisher")
public class PublisherControllerIona extends HibernateCRUDController<Publisher, PublisherDto> {

}
