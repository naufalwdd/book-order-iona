package com.example.BookOrderNaufal.controllersIona;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.dtos.BookDto;
import com.example.BookOrderNaufal.models.Book;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/api/iona/book")
public class BookControllerIona extends HibernateCRUDController<Book, BookDto>{

}
