package com.example.BookOrderNaufal.controllersIona;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.exception.ResourceNotFoundException;
import com.example.BookOrderNaufal.dtos.BookDto;
import com.example.BookOrderNaufal.dtos.BookRatingsDto;
import com.example.BookOrderNaufal.dtos.ReviewerDto;
import com.example.BookOrderNaufal.models.Book;
import com.example.BookOrderNaufal.models.BookRatings;
import com.example.BookOrderNaufal.models.Reviewer;
import com.example.BookOrderNaufal.repositories.AuthorRepository;
import com.example.BookOrderNaufal.repositories.BookRepository;
import com.example.BookOrderNaufal.repositories.ReviewerRepository;
import com.io.iona.core.enums.ActionFlow;
import com.io.iona.implementations.pagination.DefaultPagingParameter;
import com.io.iona.springboot.actionflows.custom.CustomAfterReadAll;
import com.io.iona.springboot.controllers.HibernateCRUDController;
import com.io.iona.springboot.sources.HibernateDataSource;
import com.io.iona.springboot.sources.HibernateDataUtility;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/ratings")
public class RatingsControllerCustom extends HibernateCRUDController<BookRatings, BookRatingsDto> 
	implements CustomAfterReadAll<BookRatings, BookRatingsDto>{

	@Autowired
	AuthorRepository authorRepo;
	
	@Autowired
	BookRepository bookRepo;
	
	@Autowired
	ReviewerRepository reviewerRepo;
	
	ModelMapper modelMapper = new ModelMapper();
	
	@Override
	public List<BookRatingsDto> afterReadAll(HibernateDataUtility hibernateDataUtility, HibernateDataSource<BookRatings, BookRatingsDto> dataSource,
			DefaultPagingParameter defaultPagingParameter) throws Exception {
		List<BookRatings> listRating = dataSource.getResult(ActionFlow.ON_READ_ALL_ITEMS, List.class);
		List<BookRatingsDto> listRatingDto = new ArrayList<BookRatingsDto>();
		
		for (BookRatings ratings : listRating) {
			BookRatingsDto ratingsDto = modelMapper.map(ratings, BookRatingsDto.class);
			Book book = bookRepo.findById(ratings.getBook().getBookId())
					.orElseThrow(() -> new ResourceNotFoundException("Book", "bookId", ratings.getBook().getBookId()));
			BookDto bookDto = modelMapper.map(book, BookDto.class);
			Reviewer reviewer = reviewerRepo.findById(ratings.getReviewer().getReviewerId())
					.orElseThrow(() -> new ResourceNotFoundException("Reviewer", "reviewerId", ratings.getReviewer().getReviewerId()));
			ReviewerDto reviewerDto = modelMapper.map(reviewer, ReviewerDto.class);
			ratingsDto.setBook(bookDto);
			ratingsDto.setReviewer(reviewerDto);
			listRatingDto.add(ratingsDto);
		}
		return listRatingDto;
	}
}
