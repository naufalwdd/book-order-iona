package com.example.BookOrderNaufal.controllersIona;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.dtos.ReviewerDto;
import com.example.BookOrderNaufal.models.Reviewer;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/api/iona/reviewer")
public class ReviewerControllerIona extends HibernateCRUDController<Reviewer, ReviewerDto>{

}
