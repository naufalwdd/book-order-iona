package com.example.BookOrderNaufal.controllersIona;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.dtos.BookRatingsDto;
import com.example.BookOrderNaufal.models.BookRatings;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/api/iona/ratings")
public class BookRatingsControllerIona extends HibernateCRUDController<BookRatings, BookRatingsDto> {

}
