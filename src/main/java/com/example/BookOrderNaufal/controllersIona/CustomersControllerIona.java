package com.example.BookOrderNaufal.controllersIona;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.dtos.CustomersDto;
import com.example.BookOrderNaufal.models.Customers;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/api/iona/customer")
public class CustomersControllerIona extends HibernateCRUDController<Customers, CustomersDto>{

}
