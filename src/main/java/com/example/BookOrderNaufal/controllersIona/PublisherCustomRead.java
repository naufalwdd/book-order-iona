package com.example.BookOrderNaufal.controllersIona;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.dtos.PublisherDtoCustom;
import com.example.BookOrderNaufal.exception.ResourceNotFoundException;
import com.example.BookOrderNaufal.models.Paper;
import com.example.BookOrderNaufal.models.Publisher;
import com.example.BookOrderNaufal.repositories.PaperRepository;
import com.example.BookOrderNaufal.repositories.PublisherRepository;
import com.io.iona.core.enums.ActionFlow;
import com.io.iona.implementations.pagination.DefaultPagingParameter;
import com.io.iona.springboot.actionflows.custom.CustomAfterReadAll;
import com.io.iona.springboot.actionflows.custom.CustomAfterViewDetail;
import com.io.iona.springboot.controllers.HibernateCRUDController;
import com.io.iona.springboot.sources.HibernateDataSource;
import com.io.iona.springboot.sources.HibernateDataUtility;

@RestController
@RequestMapping("/readAfterPublisher")
public class PublisherCustomRead extends HibernateCRUDController<Publisher, PublisherDtoCustom>
	implements CustomAfterReadAll<Publisher, PublisherDtoCustom>, CustomAfterViewDetail<Publisher, PublisherDtoCustom> {
	
	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	PublisherRepository publisherRepo;
	
	@Autowired
	PaperRepository paperRepo;
	
	@Override
    public List<PublisherDtoCustom> afterReadAll(HibernateDataUtility hibernateDataUtility, HibernateDataSource<Publisher, PublisherDtoCustom> dataSource, DefaultPagingParameter defaultPagingParameter) throws Exception {
        List<Publisher> publishers = dataSource.getResult(ActionFlow.ON_READ_ALL_ITEMS, List.class);        
        List<PublisherDtoCustom> dtos = new ArrayList<>();
        
        for (Publisher publisher : publishers) {
            PublisherDtoCustom dto = modelMapper.map(publisher, PublisherDtoCustom.class);
            Paper paper = paperRepo.findById(publisher.getPaper().getPaperId()).orElseThrow(() -> new ResourceNotFoundException("Paper", "id", publisher.getPaper().getPaperId()));
            dto.setQualityName(paper.getQualityName());
            dtos.add(dto);
        }
        return dtos;
    }
	
	@Override
    public PublisherDtoCustom afterViewDetail(HibernateDataUtility hibernateDataUtility, HibernateDataSource<Publisher, PublisherDtoCustom> dataSource) throws Exception {
        Publisher publisher = dataSource.getDataModel();
        PublisherDtoCustom publisherDto = modelMapper.map(publisher, PublisherDtoCustom.class);
        Paper paper = paperRepo.findById(publisher.getPaper().getPaperId())
        		.orElseThrow(() -> new ResourceNotFoundException("Paper", "paperId", publisher.getPaper().getPaperId()));
        publisherDto.setPaperId(paper.getPaperId());
        publisherDto.setQualityName(paper.getQualityName());
        return publisherDto;
    }
}
