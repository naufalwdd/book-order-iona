package com.example.BookOrderNaufal.controllersIona;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.dtos.PaperDto;
import com.example.BookOrderNaufal.models.Paper;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/api/iona/paper")
public class PaperControllerIona extends HibernateCRUDController<Paper, PaperDto> {

}
