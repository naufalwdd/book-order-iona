package com.example.BookOrderNaufal.controllersIona;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.dtos.AuthorDto;
import com.example.BookOrderNaufal.models.Author;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/api/iona/author")
public class AuthorControllerIona extends HibernateCRUDController<Author, AuthorDto>{

}
