package com.example.BookOrderNaufal.ols;

import com.example.BookOrderNaufal.models.Paper;
import com.io.iona.core.data.annotations.OptionListKey;

public class PublisherOLO {

	@OptionListKey
	private Long publisherId;
	private String companyName;
	private String country;
	private Paper paper;
	
	
	public Long getPublisherId() {
		return publisherId;
	}
	public void setPublisherId(Long publisherId) {
		this.publisherId = publisherId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Paper getPaper() {
		return paper;
	}
	public void setPaper(Paper paper) {
		this.paper = paper;
	}
	
	
}
