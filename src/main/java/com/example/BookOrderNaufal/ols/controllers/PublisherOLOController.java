package com.example.BookOrderNaufal.ols.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.models.Publisher;
import com.example.BookOrderNaufal.ols.PublisherOLO;
import com.io.iona.springboot.controllers.HibernateOptionListController;

@RestController
@RequestMapping("/ol/publisher")
public class PublisherOLOController extends HibernateOptionListController<Publisher, PublisherOLO> {

}
