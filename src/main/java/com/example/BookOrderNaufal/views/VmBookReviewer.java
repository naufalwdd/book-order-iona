package com.example.BookOrderNaufal.views;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.Immutable;

@Entity
@Immutable
@Table
public class VmBookReviewer {

	private Long book_id;
	private String title;
	private String authorFirstName;
	private String authorLastName;
	private BigDecimal price;
	private Long reviewer_id;
	
	@Id
	@Column(name = "book_id", unique = true, nullable = false)
	public Long getBook_id() {
		return book_id;
	}
	public void setBook_id(Long book_id) {
		this.book_id = book_id;
	}
	
	@Column(name = "title")
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	@Column(name = "price")
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
	@Column(name = "reviewer_id")
	public Long getReviewer_id() {
		return reviewer_id;
	}
	public void setReviewer_id(Long reviewer_id) {
		this.reviewer_id = reviewer_id;
	}
	
	@Column(name = "first_name")
	public String getAuthorFirstName() {
		return authorFirstName;
	}
	public void setAuthorFirstName(String authorFirstName) {
		this.authorFirstName = authorFirstName;
	}
	
	@Column(name = "last_name")
	public String getAuthorLastName() {
		return authorLastName;
	}
	public void setAuthorLastName(String authorLastName) {
		this.authorLastName = authorLastName;
	}
	
}
