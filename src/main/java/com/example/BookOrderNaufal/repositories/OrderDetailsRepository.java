package com.example.BookOrderNaufal.repositories;

import java.math.BigDecimal;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.BookOrderNaufal.models.OrderDetails;
import com.example.BookOrderNaufal.models.OrderDetailsKey;

public interface OrderDetailsRepository extends JpaRepository<OrderDetails, OrderDetailsKey> {
	
	// CREATE
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO order_details(book_id, order_id, discount, quantity, tax) "
			+ "VALUES (:bookId, :orderId, :discount, :quantity, :tax)", nativeQuery = true)
	public void createOrderDetails(@Param("bookId") Long bookId, @Param("orderId") Long orderId,
			@Param("discount") BigDecimal discount, @Param("quantity") int quantity,
			@Param("tax") BigDecimal tax);
	
	// READ ALL
	@Query(value = "SELECT * FROM order_details", nativeQuery = true)
	public List<OrderDetails> findAllOrderDetails();
	
	// READ SINGLE
	@Query(value = "SELECT * FROM order_details WHERE order_id=:orderId AND book_id=:bookId",
			nativeQuery = true)
	public OrderDetails findOrderDetails(@Param("orderId") Long orderId, @Param("bookId") Long bookId);
	
	// UPDATE
	@Transactional
	@Modifying
	@Query(value = "UPDATE order_details SET book_id=:bookId, order_id=:orderId, discount=:discount, "
			+ "quantity=:quantity, tax=:tax WHERE order_id=:orderId AND book_id=:bookId", nativeQuery = true)
	public void updateOrderDetails(@Param("bookId") Long bookId, @Param("orderId") Long orderId,
			@Param("discount") BigDecimal discount, @Param("quantity") int quantity,
			@Param("tax") BigDecimal tax);
	
	// DELETE
	@Transactional
	@Modifying
	@Query(value = "DELETE FROM order_details WHERE order_id=:orderId AND book_id=:bookId", nativeQuery = true)
	public void deleteOrderDetails(@Param("orderId") Long orderId, @Param("bookId") Long bookId);
}
