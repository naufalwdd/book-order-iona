package com.example.BookOrderNaufal.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.BookOrderNaufal.models.BookRatings;

public interface BookRatingsRepository extends JpaRepository<BookRatings, Long>{

	// CREATE
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO book_ratings(rating_score, book_id, reviewer_id) "
			+ "VALUES(:ratingScore, :bookId, :reviewerId)", nativeQuery=true)
	public void createRating(@Param("ratingScore") int ratingScore, 
			@Param("bookId") Long bookId, @Param("reviewerId") Long reviewerId);
	
	// READ ALL
	@Query(value = "SELECT * FROM book_ratings", nativeQuery=true)
	public List<BookRatings> findAllRating();
	
	// READ SINGLE
	@Query(value = "SELECT * FROM book_ratings WHERE book_rating_id=:bookRatingId", 
			nativeQuery=true)
	public BookRatings findRating(@Param("bookRatingId") Long bookRatingId);
	
	// UPDATE
	@Transactional
	@Modifying
	@Query(value = "UPDATE book_ratings SET rating_score=:ratingScore, "
			+ "book_id=:bookId, reviewer_id=:reviewerId WHERE book_rating_id=:bookRatingId", nativeQuery=true)
	public void updateRating(@Param("bookRatingId") Long bookRatingId, @Param("ratingScore") int ratingScore, 
			@Param("bookId") Long bookId, @Param("reviewerId") Long reviewerId);
	
	// DELETE
	@Transactional
	@Modifying
	@Query(value = "DELETE FROM book_ratings WHERE book_rating_id=:bookRatingId", nativeQuery=true)
	public void deleteRating(@Param("bookRatingId") Long bookRaingId);
}
