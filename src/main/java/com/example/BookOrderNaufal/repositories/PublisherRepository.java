package com.example.BookOrderNaufal.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.BookOrderNaufal.models.Publisher;

public interface PublisherRepository extends JpaRepository<Publisher, Long> {
	
	// CREATE
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO publisher(company_name, country, paper_id) VALUES"
			+ "(:companyName, :country, :paperId)", nativeQuery = true)
	public void createPublisher(@Param("companyName") String companyName,
			@Param("country") String country, @Param("paperId") Long paperId);
	
	// READ ALL
	@Query(value = "select * from publisher", nativeQuery = true)
	public List<Publisher> findAllPublisher();
	
	// READ SINGLE
	@Query(value = "SELECT * FROM publisher WHERE publisher_id=:publisherId", nativeQuery=true)
	public Publisher findPublisher(@Param("publisherId") Long publisherId);
	
	// UPDATE
	@Transactional
	@Modifying
	@Query(value = "UPDATE publisher SET company_name=:companyName, country=:country, paper_id=:paperId WHERE publisher_id =:publisherId",
	nativeQuery = true)
	public void updatePublisher(@Param("publisherId") Long publisherId, @Param("companyName") String companyName, 
			@Param("country") String country, @Param("paperId") Long paperId);
	
	// DELETE
	@Transactional
	@Modifying
	@Query(value = "DELETE FROM publisher WHERE publisher_id=:publisherId",nativeQuery=true)
	public void deletePublisher(@Param("publisherId") Long publisherId);
	
}
