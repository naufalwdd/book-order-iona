package com.example.BookOrderNaufal.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.BookOrderNaufal.models.Author;

public interface AuthorRepository extends JpaRepository<Author, Long> {

	// CREATE 
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO author(age, country, first_name, last_name, gender, "
			+ "rating) VALUES (:age, :country, :firstName, :lastName, :gender,"
			+ ":rating)",nativeQuery=true)
	public void createAuthor(@Param("age") int age, @Param("country") String country, 
			@Param("firstName") String firstName, @Param("lastName") String lastName, @Param("gender") String gender,
			@Param("rating") String rating);
	
	// READ ALL
	@Query(value = "SELECT * FROM author", nativeQuery=true)
	public List<Author> findAllAuthor();
	
	// READ SINGLE
	@Query(value = "SELECT * FROM author WHERE author_id=:authorId",nativeQuery=true)
	public Author findAuthor(@Param("authorId") Long authorId);
	
	// UPDATE
	@Transactional
	@Modifying
	@Query(value = "UPDATE author SET age=:age, country=:country, first_name=:firstName, "
			+ "last_name=:lastName, gender=:gender, rating=:rating WHERE author_id=:authorId", nativeQuery=true)
	public void updateAuthor(@Param("authorId") Long authorId, @Param("age") int age, @Param("country") String country,
			@Param("firstName") String firstName, @Param("lastName") String lastName, @Param("gender") String gender, 
			@Param("rating") String rating);
	
	// DELETE
	@Transactional
	@Modifying
	@Query(value = "DELETE FROM author WHERE author_id=:authorId", nativeQuery=true)
	public void deleteAuthor(@Param("authorId") Long authorId);
}
