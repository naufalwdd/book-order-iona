package com.example.BookOrderNaufal.repositories;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.BookOrderNaufal.models.Book;

public interface BookRepository extends JpaRepository<Book, Long>{
	
	// CREATE
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO book(price, release_date, title, author_id, publisher_id) "
			+ "VALUES (:price, :releaseDate, :title, :authorId, :publisherId)",
			nativeQuery = true)
	public void createBook(@Param("price") BigDecimal price, @Param("releaseDate") Date releaseDate,
			@Param("title") String title, @Param("authorId") Long authorId, @Param("publisherId") Long publisherId);
	
	// READ ALL
	@Query(value = "SELECT * FROM book", nativeQuery = true)
	public List<Book> findAllBook();
	
	// READ SINGLE
	@Query (value = "SELECT * FROM book WHERE book_id =:bookId", nativeQuery = true)
	public Book findBook(@Param("bookId") Long bookId);
	
	// UPDATE
	@Transactional
	@Modifying
	@Query (value = "UPDATE book SET price=:price, release_date=:releaseDate, title=:title, "
			+ "author_id=:authorId, publisher_id=:publisherId WHERE book_id=:bookId",
			nativeQuery = true)
	public void updateBook(@Param("bookId") Long bookId, @Param("price") BigDecimal price, @Param("releaseDate") Date releaseDate,
			@Param("title") String title, @Param("authorId") Long authorId, @Param("publisherId") Long publisherId);
	
	// DELETE
	@Transactional
	@Modifying
	@Query (value = "DELETE FROM book WHERE book_id =:bookId", nativeQuery = true)
	public void deleteBook(@Param("bookId") Long bookId);
}
