package com.example.BookOrderNaufal.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.BookOrderNaufal.models.Reviewer;

public interface ReviewerRepository extends JpaRepository<Reviewer, Long> {
	
	// CREATE
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO reviewer(country, reviewer_name, verified) "
			+ "VALUES(:country, :reviewerName, :verified)", nativeQuery=true)
	public void createReviewer(@Param("country") String country, 
			@Param("reviewerName") String reviewerName, @Param("verified") boolean verified);
	
	// READ ALL
	@Query(value = "SELECT * FROM reviewer", nativeQuery=true)
	public List<Reviewer> findAllReviewer();
	
	// READ SINGLE
	@Query(value = "SELECT * FROM reviewer WHERE reviewer_id=:reviewerId", nativeQuery=true)
	public Reviewer findReviewer(@Param("reviewerId") Long reviewerId);
	
	// UPDATE
	@Transactional
	@Modifying
	@Query(value = "UPDATE reviewer SET country=:country, reviewer_name=:reviewerName, "
			+ "verified=:verified WHERE reviewer_id=:reviewerId", nativeQuery=true)
	public void updateReviewer(@Param("reviewerId") Long reviewerId, @Param("country") String country, 
			@Param("reviewerName") String reviewerName, @Param("verified") boolean verified);
	
	// DELETE
	@Transactional
	@Modifying
	@Query(value = "DELETE FROM reviewer WHERE reviewer_id=:reviewerId", nativeQuery=true)
	public void deleteReviewer(@Param("reviewerId") Long reviewerId);
}
