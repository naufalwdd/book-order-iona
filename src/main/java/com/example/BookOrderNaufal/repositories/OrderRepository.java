package com.example.BookOrderNaufal.repositories;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.BookOrderNaufal.models.Order;

public interface OrderRepository extends JpaRepository<Order, Long>{

	// CREATE
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO orders(order_date, total_order, customer_id) "
			+ "VALUES (:orderDate, :totalOrder, :customerId)", nativeQuery=true)
	public void createOrder(@Param("orderDate") Date orderDate, @Param("totalOrder") BigDecimal totalOrder, 
			@Param("customerId") Long customerId);
	
	// READ ALL
	@Query(value = "SELECT * FROM orders", nativeQuery=true)
	public List<Order> findAllOrder();
	
	// READ SINGLE
	@Query(value = "SELECT * FROM orders WHERE order_id=:orderId", nativeQuery=true)
	public Order findOrder(@Param("orderId") Long orderId);
	
	// UPDATE
	@Transactional
	@Modifying
	@Query(value = "UPDATE orders SET order_date=:orderDate, total_order=:totalOrder, "
			+ "customer_id=:customerId WHERE order_id=:orderId", nativeQuery=true)
	public void updateOrder(@Param("orderId") Long orderId, @Param("orderDate") Date orderDate, 
			@Param("totalOrder") BigDecimal totalOrder, @Param("customerId") Long custoemerId);
	
	// DELETE
	@Transactional
	@Modifying
	@Query(value = "DELETE FROM orders WHERE order_id=:orderId", nativeQuery=true)
	public void deleteOrder(@Param("orderId") Long orderId);
}
