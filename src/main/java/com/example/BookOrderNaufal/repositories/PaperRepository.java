package com.example.BookOrderNaufal.repositories;

import java.math.BigDecimal;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.BookOrderNaufal.models.Paper;

public interface PaperRepository extends JpaRepository<Paper, Long> {

	// CREATE
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO paper(quality_name, paper_price) VALUES (:qualityName, :paperPrice)",
	nativeQuery = true)
	public void createPaper(@Param("qualityName") String qualityName,
			@Param("paperPrice") BigDecimal paperPrice);
	
	// READ ALL
	@Query(value = "SELECT * FROM paper", nativeQuery = true)
	public List<Paper> findAllPaper();
	
	// READ SINGLE
	@Query (value = "SELECT * FROM paper WHERE paper_id =:paperId", nativeQuery = true)
	public Paper findPaper(@Param("paperId") Long paperId);
	
	// UPDATE
	@Transactional
	@Modifying
	@Query(value = "UPDATE paper SET quality_name=:qualityName, paper_price=:paperPrice WHERE paper_id =:paperId",
	nativeQuery = true)
	public void updatePaper(@Param("paperId") Long paperId, @Param("qualityName") String qualityName, 
			@Param("paperPrice") BigDecimal paperPrice);
	
	// DELETE
	@Transactional
	@Modifying
	@Query (value = "DELETE FROM paper WHERE paper_id =:paperId", nativeQuery = true)
	public void deletePaper(@Param("paperId") Long paperId);
}
