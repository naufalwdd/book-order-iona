package com.example.BookOrderNaufal.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.BookOrderNaufal.models.Customers;

public interface CustomersRepository extends JpaRepository<Customers, Long>{

	// CREATE
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO customers(address, country, customer_name, email, "
			+ "phone_number, postal_code) VALUES (:address, :country, :customerName, "
			+ ":email, :phoneNumber, :postalCode)",nativeQuery=true)
	public void createCustomers(@Param("address") String address, 
			@Param("country") String country, @Param("customerName") String name, 
			@Param("email") String email,@Param("phoneNumber") String phoneNumber, @Param("postalCode") String postalCode);
	
	// READ ALL
	@Query(value = "SELECT * FROM customers", nativeQuery=true)
	public List<Customers> findAllCustomers();
	
	// READ SINGLE
	@Query(value = "SELECT * FROM customers WHERE customer_id=:customerId",nativeQuery=true)
	public Customers findCustomer(@Param("customerId") Long customerId);
	
	// UPDATE
	@Transactional
	@Modifying
	@Query(value = "UPDATE customers SET address=:address, country=:country, "
			+ "customer_name=:customerName, email=:email, phone_number=:phoneNumber, postal_code=:postalCode "
			+ "WHERE customer_id=:customerId",nativeQuery=true)
	public void updateCustomers(@Param("customerId") Long customerId, @Param("address") String address, @Param("country") String country, 
			@Param("customerName") String customerName, @Param("email") String email, @Param("phoneNumber") String phoneNumber, 
			@Param("postalCode") String postalCode);
	
	// DELETE
	@Transactional
	@Modifying
	@Query(value = "DELETE FROM customers WHERE customer_id=:customerId", nativeQuery=true)
	public void deleteCustomers(@Param("customerId") Long customerId);
}
