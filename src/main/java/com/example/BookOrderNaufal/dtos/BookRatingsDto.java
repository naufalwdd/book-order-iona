package com.example.BookOrderNaufal.dtos;

public class BookRatingsDto {
	private Long bookRatingId;
	private BookDto book;
	private ReviewerDto reviewer;
	private int ratingScore;
	
	// Constructor
	public BookRatingsDto() {
		// TODO Auto-generated constructor stub
	}

	public BookRatingsDto(Long bookRatingId, BookDto book, ReviewerDto reviewer, int ratingScore) {
		super();
		this.bookRatingId = bookRatingId;
		this.book = book;
		this.reviewer = reviewer;
		this.ratingScore = ratingScore;
	}
	
	// Getter Setter
	public Long getBookRatingId() {
		return bookRatingId;
	}

	public void setBookRatingId(Long bookRatingId) {
		this.bookRatingId = bookRatingId;
	}

	public BookDto getBook() {
		return book;
	}

	public void setBook(BookDto book) {
		this.book = book;
	}

	public ReviewerDto getReviewer() {
		return reviewer;
	}

	public void setReviewer(ReviewerDto reviewer) {
		this.reviewer = reviewer;
	}

	public int getRatingScore() {
		return ratingScore;
	}

	public void setRatingScore(int ratingScore) {
		this.ratingScore = ratingScore;
	}
	
}
