package com.example.BookOrderNaufal.dtos;

public class PublisherDtoCustom {
	private Long publisherId;
	private String companyName;
	private String country;
	private Long paperId;
	private String qualityName;
	
	public PublisherDtoCustom() {
		super();
	}
	public PublisherDtoCustom(Long publisherId, String companyName, String country, Long paperId, String qualityName) {
		super();
		this.publisherId = publisherId;
		this.companyName = companyName;
		this.country = country;
		this.paperId = paperId;
		this.qualityName = qualityName;
	}

	public Long getPublisherId() {
		return publisherId;
	}
	public void setPublisherId(Long publisherId) {
		this.publisherId = publisherId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Long getPaperId() {
		return paperId;
	}
	public void setPaperId(Long paperId) {
		this.paperId = paperId;
	}
	public String getQualityName() {
		return qualityName;
	}
	public void setQualityName(String qualityName) {
		this.qualityName = qualityName;
	}
	
	
}
