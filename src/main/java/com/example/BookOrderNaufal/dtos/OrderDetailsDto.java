package com.example.BookOrderNaufal.dtos;

import java.math.BigDecimal;

import com.example.BookOrderNaufal.models.OrderDetailsKey;

public class OrderDetailsDto {
	private OrderDetailsKey orderKey;
	private int quantity;
	private BigDecimal discount;
	private BigDecimal tax;
	private BookDto book;
	private OrderDto order;
	
	// Constructor
	public OrderDetailsDto() {
		// TODO Auto-generated constructor stub
	}

	public OrderDetailsDto(OrderDetailsKey orderKey, int quantity, BigDecimal discount, BigDecimal tax, BookDto book, OrderDto order) {
		super();
		this.orderKey = orderKey;
		this.quantity = quantity;
		this.discount = discount;
		this.tax = tax;
		this.book = book;
		this.order = order;
	}

	// Getter Setter
	public OrderDetailsKey getOrderKey() {
		return orderKey;
	}

	public void setOrderKey(OrderDetailsKey orderKey) {
		this.orderKey = orderKey;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public BigDecimal getTax() {
		return tax;
	}

	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}

	public BookDto getBook() {
		return book;
	}

	public void setBook(BookDto book) {
		this.book = book;
	}

	public OrderDto getOrder() {
		return order;
	}

	public void setOrder(OrderDto order) {
		this.order = order;
	}
	
	
}
