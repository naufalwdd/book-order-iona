package com.example.BookOrderNaufal.dtos;

import java.math.BigDecimal;
import java.util.Date;

public class BookDto {
	private Long bookId;
	private String title;
	private Date releaseDate;
	private BigDecimal price;
	private PublisherDto publisher;
	private AuthorDto author;
	
	// Constructor
	public BookDto() {
		// TODO Auto-generated constructor stub
	}

	public BookDto(Long bookId, String title, Date releaseDate, BigDecimal price, PublisherDto publisher, AuthorDto author) {
		super();
		this.bookId = bookId;
		this.title = title;
		this.releaseDate = releaseDate;
		this.price = price;
		this.publisher = publisher;
		this.author = author;
	}

	// Getter Setter
	public Long getBookId() {
		return bookId;
	}

	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public PublisherDto getPublisher() {
		return publisher;
	}

	public void setPublisher(PublisherDto publisher) {
		this.publisher = publisher;
	}

	public AuthorDto getAuthor() {
		return author;
	}

	public void setAuthor(AuthorDto author) {
		this.author = author;
	}
	
}
