package com.example.BookOrderNaufal.view.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.view.dtos.VmBookReviewerDto;
import com.example.BookOrderNaufal.views.VmBookReviewer;
import com.io.iona.springboot.controllers.HibernateViewController;

@RestController
@RequestMapping("api/viewBookReviewer")
public class VmBookReviewerController extends HibernateViewController<VmBookReviewer, VmBookReviewerDto> {

}
