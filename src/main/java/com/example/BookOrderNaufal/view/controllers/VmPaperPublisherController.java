package com.example.BookOrderNaufal.view.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.view.dtos.VmPaperPublisherDto;
import com.example.BookOrderNaufal.views.VmPaperPublisher;
import com.io.iona.springboot.controllers.HibernateViewController;

@RestController
@RequestMapping("api/viewPaperPublisher")
public class VmPaperPublisherController extends HibernateViewController<VmPaperPublisher, VmPaperPublisherDto> {

}
