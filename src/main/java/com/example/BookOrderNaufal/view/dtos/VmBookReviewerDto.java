package com.example.BookOrderNaufal.view.dtos;

import java.math.BigDecimal;

public class VmBookReviewerDto {

	private Long book_id;
	private String title;
	private String authorFirstName;
	private String authorLastName;
	private BigDecimal price;
	private Long reviewer_id;
	
	public Long getBook_id() {
		return book_id;
	}
	public void setBook_id(Long book_id) {
		this.book_id = book_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public Long getReviewer_id() {
		return reviewer_id;
	}
	public void setReviewer_id(Long reviewer_id) {
		this.reviewer_id = reviewer_id;
	}
	public String getAuthorFirstName() {
		return authorFirstName;
	}
	public void setAuthorFirstName(String authorFirstName) {
		this.authorFirstName = authorFirstName;
	}
	public String getAuthorLastName() {
		return authorLastName;
	}
	public void setAuthorLastName(String authorLastName) {
		this.authorLastName = authorLastName;
	}
	
}
