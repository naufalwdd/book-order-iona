package com.example.BookOrderNaufal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookOrderNaufalApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookOrderNaufalApplication.class, args);
	}

}
