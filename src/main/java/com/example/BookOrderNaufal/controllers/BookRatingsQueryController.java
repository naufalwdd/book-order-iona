package com.example.BookOrderNaufal.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.dtos.BookRatingsDto;
import com.example.BookOrderNaufal.models.BookRatings;
import com.example.BookOrderNaufal.repositories.BookRatingsRepository;

@RestController
@RequestMapping("api/ratingquery")
public class BookRatingsQueryController {

	@Autowired
	BookRatingsRepository ratingRepo;
	
	ModelMapper modelMapper = new ModelMapper();
	
	// CREATE
	@PostMapping("/create")
	public Map<String, Object> createRating(@Valid @RequestBody BookRatingsDto body) {
		Map<String, Object> result = new HashMap<String, Object>();
		ratingRepo.createRating(body.getRatingScore(), 
				body.getBook().getBookId(), body.getReviewer().getReviewerId());
		result.put("Message", "Rating Created Successfully.");
		result.put("Data", body);
		return result;
	}
	
	// READ ALL
	@GetMapping("/readAll")
	public Map<String, Object> getAllRatings() {
		Map<String, Object> result = new HashMap<String, Object>();
		List<BookRatings> listRatingEntity = ratingRepo.findAllRating();
		List<BookRatingsDto> listRatingDto = new ArrayList<BookRatingsDto>();
		
		for (BookRatings ratingEntity : listRatingEntity) {
			BookRatingsDto ratingDto = new BookRatingsDto();
			ratingDto = modelMapper.map(ratingEntity, BookRatingsDto.class);
			listRatingDto.add(ratingDto);
		}
		result.put("Message", "All Ratings Read Successfully");
		result.put("Total Data", listRatingDto.size());
		result.put("Data", listRatingDto);
		return result;
	}
	
	// READ SINGLE
	@GetMapping("/read")
	public Map<String, Object> getRating(@RequestParam(name = "bookRatingId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		BookRatings ratingEntity = ratingRepo.findRating(id);
		BookRatingsDto ratingDto = new BookRatingsDto();
		ratingDto = modelMapper.map(ratingEntity, BookRatingsDto.class);
		
		result.put("Message", "Rating Read Successfully.");
		result.put("Data", ratingDto);
		return result;
	}
	
	// UPDATE
	@PutMapping("/update")
	public Map<String, Object> updateRating(@Valid @RequestBody BookRatingsDto body, 
			@RequestParam(name = "bookRatingId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		ratingRepo.updateRating(id, body.getRatingScore(), body.getBook().getBookId(), body.getReviewer().getReviewerId());
		
		result.put("Message", "Rating Updated Successfully.");
		result.put("Data", body);
		return result;
	}
	
	// DELETE
	@DeleteMapping("/delete")
	public Map<String, Object> deleteRating(@RequestParam(name = "bookRatingId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		BookRatings ratingEntity = ratingRepo.findRating(id);
		BookRatingsDto ratingDto = new BookRatingsDto();
		ratingDto = modelMapper.map(ratingEntity, BookRatingsDto.class);
		ratingRepo.deleteRating(id);
		
		result.put("Message", "Rating Deleted Successfully.");
		result.put("Data", ratingDto);
		return result;
	}
}
