package com.example.BookOrderNaufal.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.dtos.OrderDto;
import com.example.BookOrderNaufal.models.Order;
import com.example.BookOrderNaufal.repositories.OrderRepository;

@RestController
@RequestMapping("/api/orderquery")
public class OrderQueryController {

	@Autowired
	OrderRepository orderRepo;
	
	ModelMapper modelMapper = new ModelMapper();
	
	// CREATE
	@PostMapping("/create")
	public Map<String, Object> createOrder(@Valid @RequestBody OrderDto body) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		orderRepo.createOrder(body.getOrderDate(), body.getTotalOrder(),
				body.getCustomers().getCustomerId());
		
		result.put("Message", "Order Created Successfully.");
		result.put("Data", body);
		return result;
	}
	
	// READ ALL
	@GetMapping("/readAll")
	public Map<String, Object> getAllOrder() {
		Map<String, Object> result = new HashMap<String, Object>();
		List<Order> listOrderEntity = orderRepo.findAllOrder();
		List<OrderDto> listOrderDto = new ArrayList<OrderDto>();
		
		for(Order orderEntity : listOrderEntity) {
			OrderDto orderDto = new OrderDto();
			orderDto = modelMapper.map(orderEntity, OrderDto.class);
			listOrderDto.add(orderDto);
		}
		result.put("Message", "All Paper Read Successfully");
		result.put("Total Data", listOrderDto.size());
		result.put("Data", listOrderDto);
		return result;
	}
	
	// READ SINGLE
	@GetMapping("/read")
	public Map<String, Object> getOrder(@RequestParam(name = "orderId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		Order orderEntity = orderRepo.findOrder(id);
		OrderDto orderDto = new OrderDto();
		orderDto = modelMapper.map(orderEntity, OrderDto.class);
		
		result.put("Message", "Order Read Successfully.");
		result.put("Data", orderDto);
		return result;
	}
	
	// UPDATE
	@PutMapping("/update")
	public Map<String, Object> updateOrder(@Valid @RequestBody OrderDto body, @RequestParam(name = "orderId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		orderRepo.updateOrder(id, body.getOrderDate(), body.getTotalOrder(), body.getCustomers().getCustomerId());
		result.put("Message", "Order Updated Successfully.");
		result.put("Data", body);
		return result;
	}
	
	// DELETE
	@DeleteMapping("/delete")
	public Map<String, Object> deleteOrder(@RequestParam(name = "orderId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		Order orderEntity = orderRepo.findOrder(id);
		OrderDto orderDto = new OrderDto();
		orderDto = modelMapper.map(orderEntity, OrderDto.class);
		orderRepo.deleteOrder(id);
		
		result.put("Message", "Order Deleted Successfully.");
		result.put("Data", orderDto);
		return result;
	}
}
