package com.example.BookOrderNaufal.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.repositories.OrderDetailsRepository;
import com.example.BookOrderNaufal.exception.ResourceNotFoundException;
import com.example.BookOrderNaufal.interfaces.DiscountRate;
import com.example.BookOrderNaufal.interfaces.TaxRate;
import com.example.BookOrderNaufal.dtos.OrderDetailsDto;
import com.example.BookOrderNaufal.models.OrderDetails;
import com.example.BookOrderNaufal.models.OrderDetailsKey;

@RestController
@RequestMapping("/api/orderdetails")
public class OrderDetailsController implements DiscountRate, TaxRate {
	ModelMapper modelMapper = new ModelMapper();
	@Autowired
	OrderDetailsRepository orderDetailsRepo;
	
	// CREATE
	@PostMapping("/create")
	public Map<String, Object> createOrderDetails(@Valid @RequestBody OrderDetailsDto body) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		OrderDetails orderDetailsEntity = modelMapper.map(body, OrderDetails.class);
		
		orderDetailsRepo.save(orderDetailsEntity);
		
		body.setOrderKey(orderDetailsEntity.getOrderKey());
		result.put("Message", "Order Details Created Successfully");
		result.put("Data", body);
		return result;
	}
	
	// CALCULATE TAX
	private BigDecimal calculateTax(OrderDetails body) {
		BigDecimal bookPrice = body.getBook().getPrice();
		BigDecimal quantity = BigDecimal.valueOf(body.getQuantity());
		BigDecimal tax = (taxRate.multiply(quantity)).multiply(bookPrice);
		
		return tax;
	}
	
	// CALCULATE ALL TAX
	@PostMapping("/tax")
	public Map<String, Object> calculateAllTax() {
		Map<String, Object> result = new HashMap<String, Object>();
		List<OrderDetails> listOrderDetails = orderDetailsRepo.findAll();
		
		for (OrderDetails orderDetailsEntity : listOrderDetails) {
			orderDetailsEntity.setTax(calculateTax(orderDetailsEntity));
			
			orderDetailsRepo.save(orderDetailsEntity);
		}
		
		result.put("Status", "All Order Tax Has Been Calculated Successfully");
		return result;
	}
	
	// CALCULATE DISCOUNT
	private BigDecimal getDetailsOfOrder(OrderDetails body) {
		Set<OrderDetails> bookDetails = body.getOrder().getOrderDetails();
		return validateTotalBookType(body, bookDetails);
    }
    
	private BigDecimal validateTotalBookType(OrderDetails body, Set<OrderDetails> bookDetails) {
		BigDecimal discount = new BigDecimal(0);
		if (bookDetails.size() >= 3) {
			discount = calculateDiscount(body, discount);
			return discount;
		}
		else {
			return discount;
		}
	}

	private BigDecimal calculateDiscount(OrderDetails body, BigDecimal discount) {
		discount = discount.add(discountRate.multiply(body.getBook().getPrice()))
				.multiply(new BigDecimal(body.getQuantity()));
		return discount;
	}
	
	// CALCULATE ALL DISCOUNT
	@PostMapping("/discount")
	private Map<String, Object> calculateAllDiscount() {
		Map<String, Object> result = new HashMap<String, Object>();
		List<OrderDetails> listOrderDetails = orderDetailsRepo.findAll();
		
		for(OrderDetails orderDetailsEntity : listOrderDetails) {
			orderDetailsEntity.setDiscount(getDetailsOfOrder(orderDetailsEntity));
			orderDetailsRepo.save(orderDetailsEntity);
		}
		result.put("Status", "All Order Discount Has Been Calculated Successfully");
		return result;
	}
	
	// GET ALL
	@GetMapping("/readAll")
	public Map<String, Object> getAllOrderDetails() {
		Map<String, Object> result = new HashMap<String, Object>();
		
		List<OrderDetails> listOrderDetailsEntity = orderDetailsRepo.findAll();
		List<OrderDetailsDto> listOrderDetailsDto = new ArrayList<OrderDetailsDto>();
		
		for (OrderDetails orderDetailsEntity : listOrderDetailsEntity) {
			OrderDetailsDto orderDetailsDto = modelMapper.map(orderDetailsEntity, OrderDetailsDto.class);
			listOrderDetailsDto.add(orderDetailsDto);
		}
		
		result.put("Message", "All Order Details Read Successfully");
		result.put("Data", listOrderDetailsDto);
		result.put("Total", listOrderDetailsDto.size());
		
		return result;
	}
	
	// GET SINGLE BY ID
	@GetMapping("/read")
	public Map<String, Object> getOrderDetails(@RequestParam(name = "orderId") Long orderId,
			@RequestParam(name = "bookId") Long bookId) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		OrderDetailsKey orderDetailsKey = new OrderDetailsKey();
		orderDetailsKey.setOrderId(orderId);
		orderDetailsKey.setBookId(bookId);
        
        OrderDetails orderDetailsEntity = orderDetailsRepo.findById(orderDetailsKey)
                .orElseThrow(() -> new ResourceNotFoundException("OrderDetails", "orderDetailId", orderDetailsKey));
		
        OrderDetailsDto orderDetailsDto = new OrderDetailsDto();
        orderDetailsDto = modelMapper.map(orderDetailsEntity, OrderDetailsDto.class);
		
		result.put("Message", "Order Details Read Successfully.");
		result.put("Data", orderDetailsDto);
		return result;
	}
	
	// UPDATE
	@PutMapping("/update")
	public Map<String, Object> updateOrderDetails(@Valid @RequestBody OrderDetailsDto body, @RequestParam(name = "orderId") Long orderId,
			@RequestParam(name = "bookId") Long bookId) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		OrderDetailsKey orderDetailsKey = new OrderDetailsKey();
		orderDetailsKey.setOrderId(orderId);
		orderDetailsKey.setBookId(bookId);
		
		OrderDetails orderDetailsEntity = orderDetailsRepo.findById(orderDetailsKey)
				.orElseThrow(() -> new ResourceNotFoundException("OrderDetails", "orderDetailId", orderDetailsKey));
		
		orderDetailsEntity = modelMapper.map(body, OrderDetails.class);
		
		orderDetailsRepo.save(orderDetailsEntity);
		
		body.setOrderKey(orderDetailsEntity.getOrderKey());
		result.put("Message", "Order Detail Updated Successfully");
		result.put("Data", body);
		return result;
	}
	
	// DELETE
	@DeleteMapping("/delete")
    public Map<String, Object> getOrderDetailsDelete(@RequestParam(name = "orderId") Long orderId
            , @RequestParam(name = "bookId")Long bookId){
        Map<String, Object> result = new HashMap<String, Object>();
        
        OrderDetailsKey orderDetailsKey = new OrderDetailsKey();
        orderDetailsKey.setOrderId(orderId);
        orderDetailsKey.setBookId(bookId);
        
        OrderDetails orderDetailsEntity = orderDetailsRepo.findById(orderDetailsKey)
                .orElseThrow(() -> new ResourceNotFoundException("OrderDetails", "orderDetailId", orderDetailsKey));
		
        OrderDetailsDto orderDetailsDto = new OrderDetailsDto();
        orderDetailsDto = modelMapper.map(orderDetailsEntity, OrderDetailsDto.class);
		
        orderDetailsRepo.deleteById(orderDetailsKey);
        
		result.put("Message", "Order Details Deleted Successfully.");
		result.put("Data Delete", orderDetailsDto);
		return result;
	}
}
