package com.example.BookOrderNaufal.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.dtos.PaperDto;
import com.example.BookOrderNaufal.models.Paper;
import com.example.BookOrderNaufal.repositories.PaperRepository;
import com.example.BookOrderNaufal.exception.ResourceNotFoundException;

@RestController
@RequestMapping("/api/paper")
public class PaperController {
	
	ModelMapper modelMapper = new ModelMapper();
	@Autowired
	PaperRepository paperRepo;
	
	// CREATE
	@PostMapping("create")
	public Map<String, Object> createPaper(@Valid @RequestBody PaperDto body) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Paper paperEntity = modelMapper.map(body, Paper.class);
		
		paperRepo.save(paperEntity);
		
		body.setPaperId(paperEntity.getPaperId());
		
		result.put("Message", "Paper Created Successfully");
		result.put("Data", body);
		return result;
	}
	
	// GET ALL
	@GetMapping("/readAll")
	public Map<String, Object> getAllPaper() {
		Map<String, Object> result = new HashMap<String, Object>();
		
		List<Paper> listPaperEntity = paperRepo.findAll();
		List<PaperDto> listPaperDto = new ArrayList<PaperDto>();
		
		for (Paper paperEntity : listPaperEntity) {
			PaperDto paperDto = modelMapper.map(paperEntity, PaperDto.class);
			listPaperDto.add(paperDto);
		}
		
		result.put("Message", "All Paper Read Successfully");
		result.put("Data", listPaperDto);
		result.put("Total", listPaperDto.size());
		
		return result;
	}
	
	// GET SINGLE BY ID
	@GetMapping("/read")
	public Map<String, Object> getPaper(@RequestParam(name = "paperId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Paper paperEntity = paperRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Paper", "paperId", id));
		
		PaperDto paperDto = modelMapper.map(paperEntity, PaperDto.class);
		
		result.put("Message", "Paper Read Successfully");
		result.put("Data", paperDto);
		return result;
	}
	
	// UPDATE
	@PutMapping("/update")
	public Map<String, Object> updatePaper(@Valid @RequestBody PaperDto body, @RequestParam(name = "paperId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Paper paperEntity = paperRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Paper", "paperId", id));
		paperEntity = modelMapper.map(body, Paper.class);
		paperEntity.setPaperId(id);
		
		paperRepo.save(paperEntity);
		
		body.setPaperId(paperEntity.getPaperId());
		result.put("Message", "Paper Updated Successfully");
		result.put("Data", body);
		return result;
	}
	
	// DELETE
	@DeleteMapping("/delete")
	public Map<String, Object> deletePaper(@RequestParam(name = "paperId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Paper paperEntity = paperRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Paper", "paperId", id));
		
		PaperDto paperDto = modelMapper.map(paperEntity, PaperDto.class);
		
		paperRepo.deleteById(id);
		
		result.put("Message", "Paper Read Successfully");
		result.put("Data", paperDto);
		return result;
	}
}
