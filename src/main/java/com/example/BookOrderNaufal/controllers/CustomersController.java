package com.example.BookOrderNaufal.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.repositories.CustomersRepository;
import com.example.BookOrderNaufal.exception.ResourceNotFoundException;
import com.example.BookOrderNaufal.dtos.CustomersDto;
import com.example.BookOrderNaufal.models.Customers;

@RestController
@RequestMapping("/api/customers")
public class CustomersController {
	ModelMapper modelMapper = new ModelMapper();
	@Autowired
	CustomersRepository customersRepo;
	
	// CREATE
	@PostMapping("/create")
	public Map<String, Object> createCustomers(@Valid @RequestBody CustomersDto body) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Customers customersEntity = modelMapper.map(body, Customers.class);
		
		customersRepo.save(customersEntity);
		
		body.setCustomerId(customersEntity.getCustomerId());
		result.put("Message", "Customer Created Successfully");
		result.put("Data", body);
		return result;
	}
	
	// GET ALL
	@GetMapping("/readAll")
	public Map<String, Object> getAllCustomers() {
		Map<String, Object> result = new HashMap<String, Object>();
		
		List<Customers> listCustomersEntity = customersRepo.findAll();
		List<CustomersDto> listCustomersDto = new ArrayList<CustomersDto>();
		
		for (Customers customersEntity : listCustomersEntity) {
			CustomersDto customersDto = modelMapper.map(customersEntity, CustomersDto.class);
			listCustomersDto.add(customersDto);
		}
		
		result.put("Message", "All Customers Read Successfully");
		result.put("Data", listCustomersDto);
		result.put("Total", listCustomersDto.size());
		
		return result;
	}
	
	// GET SINGLE BY ID
	@GetMapping("/read")
	public Map<String, Object> getCustomers(@RequestParam(name = "customerId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Customers customersEntity = customersRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Customers", "customerId", id));
		
		CustomersDto customersDto = modelMapper.map(customersEntity, CustomersDto.class);
		
		result.put("Message", "Customer Read Successfully");
		result.put("Data", customersDto);
		return result;
	}
	
	// UPDATE
	@PutMapping("/update")
	public Map<String, Object> updateCustomer(@Valid @RequestBody CustomersDto body, @RequestParam(name = "customerId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Customers customersEntity = customersRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Customers", "customerId", id));
		customersEntity = modelMapper.map(body, Customers.class);
		customersEntity.setCustomerId(id);
		
		customersRepo.save(customersEntity);
		
		body.setCustomerId(customersEntity.getCustomerId());
		result.put("Message", "Customer Updated Successfully");
		result.put("Data", body);
		return result;
	}
	
	// DELETE
	@DeleteMapping("/delete")
	public Map<String, Object> deleteCustomer(@RequestParam(name = "customerId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Customers customersEntity = customersRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Customers", "customerId", id));
		
		CustomersDto customersDto = modelMapper.map(customersEntity, CustomersDto.class);
		
		customersRepo.deleteById(id);
		
		result.put("Message", "Customer Deleted Successfully");
		result.put("Data Deleted", customersDto);
		return result;
	}
}
