package com.example.BookOrderNaufal.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.dtos.PublisherDto;
import com.example.BookOrderNaufal.models.Publisher;
import com.example.BookOrderNaufal.repositories.PublisherRepository;

@RestController
@RequestMapping("/api/publisherquery")
public class PublisherQueryController {
	
	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	PublisherRepository publisherRepo;
	
	// CREATE
	@PostMapping("/create")
	public Map<String, Object> createPublisher(@Valid @RequestBody PublisherDto body) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		publisherRepo.createPublisher(body.getCompanyName(), 
				body.getCountry(), body.getPaperId());
		
		result.put("Message", "Publisher Created Successfully");
		result.put("Data", body);
		return result;
	}
	
	// READ ALL
	@GetMapping("/readAll")
	public Map<String, Object> getAllPublisher() {
		Map<String, Object> result = new HashMap<String, Object>();
		List<Publisher> listPublisherEntity = publisherRepo.findAllPublisher();
		List<PublisherDto> listPublisherDto = new ArrayList<PublisherDto>();
		
		for(Publisher publisherEntity : listPublisherEntity) {
			PublisherDto publisherDto = new PublisherDto();
			publisherDto = modelMapper.map(publisherEntity, PublisherDto.class);
			listPublisherDto.add(publisherDto);
		}
		result.put("Message", "All Publisher Read Successfully.");
		result.put("Data", listPublisherDto);
		return result;
	}
	
	// READ SINGLE
	@GetMapping("/read")
	public Map<String, Object> getPublisher(@RequestParam(name = "publisherId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		Publisher publisherEntity = publisherRepo.findPublisher(id);
		PublisherDto publisherDto = new PublisherDto();
		
		publisherDto = modelMapper.map(publisherEntity, PublisherDto.class);
		
		result.put("Message", "Publisher Read Successfully");
		result.put("Data", publisherDto);
		return result;
	}
	
	// UPDATE 
	@PutMapping("/update")
	public Map<String, Object> updatePublisher(@Valid @RequestBody PublisherDto body,
			@RequestParam(name = "publisherId") Long publisherId) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		publisherRepo.updatePublisher(publisherId, body.getCompanyName(), 
				body.getCountry(), body.getPaperId());
		
		result.put("Message", "Publisher Updated Successfully");
		result.put("Data", body);
		return result;
	}
	
	// DELETE
	@DeleteMapping("/delete")
	public Map<String, Object> deletePublisher(@RequestParam(name = "publisherId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		Publisher publisherEntity = publisherRepo.findPublisher(id);
		PublisherDto publisherDto = new PublisherDto();
		
		publisherDto = modelMapper.map(publisherEntity, PublisherDto.class);
		
		result.put("Message", "Publisher Deleted Successfully");
		result.put("Data", publisherDto);
		return result;
	}
}
