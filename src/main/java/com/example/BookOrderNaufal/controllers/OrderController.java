package com.example.BookOrderNaufal.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.repositories.OrderRepository;
import com.example.BookOrderNaufal.exception.ResourceNotFoundException;
import com.example.BookOrderNaufal.dtos.OrderDto;
import com.example.BookOrderNaufal.models.Order;
import com.example.BookOrderNaufal.models.OrderDetails;

@RestController
@RequestMapping("/api/order")
public class OrderController {
	ModelMapper modelMapper = new ModelMapper();
	@Autowired
	OrderRepository orderRepo;
	
	// CREATE
	@PostMapping("/create")
	public Map<String, Object> createOrder(@Valid @RequestBody OrderDto body) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Order orderEntity = modelMapper.map(body, Order.class);
		
		orderRepo.save(orderEntity);
		
		body.setOrderId(orderEntity.getOrderId());
		result.put("Message", "Order Created Successfully");
		result.put("Data", body);
		return result;
	}
	
	// CALCULATE TOTAL ORDER
    public BigDecimal calculateTotalOrder(Order order) {
        
        BigDecimal totalOrder = new BigDecimal(0);
        
        for (OrderDetails orderDetailsEntity : order.getOrderDetails()) {
            totalOrder = totalOrder.add((orderDetailsEntity.getBook().getPrice()
            		.multiply(new BigDecimal(orderDetailsEntity.getQuantity())))
            		.add(orderDetailsEntity.getTax())
            		.subtract(orderDetailsEntity.getDiscount()));
        }
        return totalOrder;
    }
	
	// CALCULATE ALL TOTAL ORDER
	@PostMapping("/totalOrder")
	private Map<String, Object> calculateAllOrder() {
		Map<String, Object> result = new HashMap<String, Object>();
		List<Order> listOrder = orderRepo.findAll();
		
		for(Order orderEntity : listOrder) {
			orderEntity.setTotalOrder(calculateTotalOrder(orderEntity));
			orderRepo.save(orderEntity);
		}
		result.put("Status", "Total Order Has Been Calculated Successfully");
		return result;
	}
	
	// GET ALL
	@GetMapping("/readAll")
	public Map<String, Object> getAllOrder() {
		Map<String, Object> result = new HashMap<String, Object>();
		
		List<Order> listOrderEntity = orderRepo.findAll();
		List<OrderDto> listOrderDto = new ArrayList<OrderDto>();
		
		for (Order orderEntity : listOrderEntity) {
			OrderDto orderDto = modelMapper.map(orderEntity, OrderDto.class);
			listOrderDto.add(orderDto);
		}
		
		result.put("Message", "All Order Read Successfully");
		result.put("Data", listOrderDto);
		result.put("Total", listOrderDto.size());
		
		return result;
	}
	
	// GET SINGLE BY ID
	@GetMapping("/read")
	public Map<String, Object> getOrder(@RequestParam(name = "orderId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Order orderEntity = orderRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Order", "orderId", id));
		
		OrderDto orderDto = modelMapper.map(orderEntity, OrderDto.class);
		
		result.put("Message", "Order Read Successfully");
		result.put("Data", orderDto);
		return result;
	}
	
	// UPDATE
	@PutMapping("/update")
	public Map<String, Object> updateOrder(@Valid @RequestBody OrderDto body, @RequestParam(name = "orderId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Order orderEntity = orderRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Order", "orderId", id));
		orderEntity = modelMapper.map(body, Order.class);
		orderEntity.setOrderId(id);
		
		orderRepo.save(orderEntity);
		
		body.setOrderId(orderEntity.getOrderId());
		result.put("Message", "Order Updated Successfully");
		result.put("Data", body);
		return result;
	}
	
	// DELETE
	@DeleteMapping("/delete")
	public Map<String, Object> deleteOrder(@RequestParam(name = "orderId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Order orderEntity = orderRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Order", "orderId", id));
		
		OrderDto orderDto = modelMapper.map(orderEntity, OrderDto.class);
		
		orderRepo.deleteById(id);
		
		result.put("Message", "Order Deleted Successfully");
		result.put("Data Deleted", orderDto);
		return result;
	}
}
