package com.example.BookOrderNaufal.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.repositories.PublisherRepository;
import com.example.BookOrderNaufal.exception.ResourceNotFoundException;
import com.example.BookOrderNaufal.dtos.PublisherDto;
import com.example.BookOrderNaufal.models.Publisher;

@RestController
@RequestMapping("/api/publisher")
public class PublisherController {
	ModelMapper modelMapper = new ModelMapper();
	@Autowired
	PublisherRepository publisherRepo;
	
	// CREATE
	@PostMapping("/create")
	public Map<String, Object> createPublisher(@Valid @RequestBody PublisherDto body) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Publisher publisherEntity = modelMapper.map(body, Publisher.class);
		
		publisherRepo.save(publisherEntity);
		
		body.setPublisherId(publisherEntity.getPublisherId());
		result.put("Message", "Publisher Created Successfully");
		result.put("Data", body);
		return result;
	}
	
	// GET ALL
	@GetMapping("/readAll")
	public Map<String, Object> getAllPublisher() {
		Map<String, Object> result = new HashMap<String, Object>();
		
		List<Publisher> listPublisherEntity = publisherRepo.findAll();
		List<PublisherDto> listPublisherDto = new ArrayList<PublisherDto>();
		
		for (Publisher publisherEntity : listPublisherEntity) {
			PublisherDto publisherDto = modelMapper.map(publisherEntity, PublisherDto.class);
			listPublisherDto.add(publisherDto);
		}
		
		result.put("Message", "All Publisher Read Successfully");
		result.put("Data", listPublisherDto);
		result.put("Total", listPublisherDto.size());
		
		return result;
	}
	
	// GET SINGLE BY ID
	@GetMapping("/read")
	public Map<String, Object> getPublisher(@RequestParam(name = "publisherId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Publisher publisherEntity = publisherRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Publisher", "publisherId", id));
		
		PublisherDto publisherDto = modelMapper.map(publisherEntity, PublisherDto.class);
		
		result.put("Message", "Publisher Read Successfully");
		result.put("Data", publisherDto);
		return result;
	}
	
	// UPDATE
	@PutMapping("/update")
	public Map<String, Object> updatePublisher(@Valid @RequestBody PublisherDto body, @RequestParam(name = "publisherId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		Publisher publisherEntity = publisherRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Publisher", "publisherId", id));
		
		publisherEntity = modelMapper.map(body, Publisher.class);
		publisherEntity.setPublisherId(id);
		
		publisherRepo.save(publisherEntity);
		
		body.setPublisherId(publisherEntity.getPublisherId());
		result.put("Message", "Publisher Updated Successfully");
		result.put("Data", body);
		return result;
	}
	
	// DELETE
	@DeleteMapping("/delete")
	public Map<String, Object> deletePublisher(@RequestParam(name = "publisherId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Publisher publisherEntity = publisherRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Publisher", "publisherId", id));
		
		PublisherDto publisherDto = modelMapper.map(publisherEntity, PublisherDto.class);
		
		publisherRepo.deleteById(id);
		
		result.put("Message", "Publisher Read Successfully");
		result.put("Data", publisherDto);
		return result;
	}
}
