package com.example.BookOrderNaufal.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.dtos.AuthorDto;
import com.example.BookOrderNaufal.models.Author;
import com.example.BookOrderNaufal.repositories.AuthorRepository;

@RestController
@RequestMapping("/api/authorquery")
public class AuthorQueryController {
	
	@Autowired
	AuthorRepository authorRepo;
	
	ModelMapper modelMapper = new ModelMapper();
	
	// CREATE
	@PostMapping("/create")
	public Map<String, Object> createAuthor(@Valid @RequestBody AuthorDto body) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		authorRepo.createAuthor(body.getAge(), body.getCountry(), 
				body.getFirstName(), body.getLastName(), body.getGender(), 
				body.getRating());
		
		result.put("Message", "Author Created Successfully.");
		result.put("Data", body);
		return result;
	}
	
	// READ ALL
	@GetMapping("/readAll")
	public Map<String, Object> getAllAuthor() {
		Map<String, Object> result = new HashMap<String, Object>();
		List<Author> listAuthorEntity = authorRepo.findAllAuthor();
		List<AuthorDto> listAuthorDto = new ArrayList<AuthorDto>();
		
		for(Author authorEntity : listAuthorEntity) {
			AuthorDto authorDto = new AuthorDto();
			authorDto = modelMapper.map(authorEntity, AuthorDto.class);
			listAuthorDto.add(authorDto);
		}
		result.put("Message", "All Author Read Successfully");
		result.put("Total Data", listAuthorDto.size());
		result.put("Data", listAuthorDto);
		return result;
	}
	
	// READ SINGLE
	@GetMapping("/read")
	public Map<String, Object> getAuthor(@RequestParam(name = "authorId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		Author authorEntity = authorRepo.findAuthor(id);
		AuthorDto authorDto = new AuthorDto();
		authorDto = modelMapper.map(authorEntity, AuthorDto.class);
		
		result.put("Message", "Author Read Successfully.");
		result.put("Data", authorDto);
		return result;
	}
	
	// UPDATE
	@PutMapping("/update")
	public Map<String, Object> updateAuthor(@Valid @RequestBody AuthorDto body, @RequestParam(name = "authorId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		authorRepo.updateAuthor(id, body.getAge(), body.getCountry(), body.getFirstName(), body.getLastName(), 
				body.getGender(), body.getRating());
		
		result.put("Message", "Author Updated Successfully.");
		result.put("Data", body);
		return result;
	}
	
	// DELETE
	@DeleteMapping("/delete")
	public Map<String, Object> deleteAuthor(@RequestParam(name = "authorId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		Author authorEntity = authorRepo.findAuthor(id);
		AuthorDto authorDto = new AuthorDto();
		
		authorDto = modelMapper.map(authorEntity, AuthorDto.class);
		authorRepo.deleteAuthor(id);
		
		result.put("Message", "Author Deleted Successfully.");
		result.put("Data", authorDto);
		return result;
	}
}
