package com.example.BookOrderNaufal.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.models.OrderDetails;
import com.example.BookOrderNaufal.repositories.OrderDetailsRepository;
import com.example.BookOrderNaufal.dtos.OrderDetailsDto;

@RestController
@RequestMapping("/api/orderdetailsquery")
public class OrderDetailsQueryController {
	
	@Autowired
	OrderDetailsRepository orderDetailsRepo;
	
	ModelMapper modelMapper = new ModelMapper();
	
	// >>>> MASIH ERROR <<<<
	// CREATE
	@PostMapping("/create")
	public Map<String, Object> createOrderDetails(@Valid @RequestBody OrderDetailsDto body) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		orderDetailsRepo.createOrderDetails(body.getOrderKey().getBookId(), body.getOrderKey().getOrderId(), 
				body.getDiscount(), body.getQuantity(), body.getTax());
		
		result.put("Message", "Order Details Created Successfully");
		result.put("Data", body);
		return result;
	}
	
	// READ ALL
	@GetMapping("/readAll")
	public Map<String, Object> getAllOrderDetails() {
		Map<String, Object> result = new HashMap<String, Object>();
		List<OrderDetails> listOrderDetailsEntity = orderDetailsRepo.findAllOrderDetails();
		List<OrderDetailsDto> listOrderDetailsDto = new ArrayList<OrderDetailsDto>();
		
		for (OrderDetails orderDetailsEntity : listOrderDetailsEntity) {
			OrderDetailsDto orderDetailsDto = new OrderDetailsDto();
			orderDetailsDto = modelMapper.map(orderDetailsEntity, OrderDetailsDto.class);
			listOrderDetailsDto.add(orderDetailsDto);
		}
		
		result.put("Message", "All OrderDetails Read Successfully");
		result.put("Total Data", listOrderDetailsDto.size());
		result.put("Data", listOrderDetailsDto);
		return result;
	}
	
	// READ SINGLE
	@GetMapping("/read")
	public Map<String, Object> getOrderDetails(@RequestParam(name = "orderId") Long orderId,
			@RequestParam(name = "bookId") Long bookId) {
		Map<String, Object> result = new HashMap<String, Object>();
		OrderDetails orderDetailsEntity = orderDetailsRepo.findOrderDetails(orderId, bookId);
		OrderDetailsDto orderDetailsDto = new OrderDetailsDto();
		
		orderDetailsDto = modelMapper.map(orderDetailsEntity, OrderDetailsDto.class);
		
		result.put("Message", "Order Details Read Successfully");
		result.put("Data", orderDetailsDto);
		return result;
	}
	
	// UPDATE
	@PutMapping("/update")
	public Map<String, Object> updateOrderDetails(@Valid @RequestBody OrderDetailsDto body,
			@RequestParam(name = "orderId") Long orderId, @RequestParam(name = "bookId") Long bookId) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		orderDetailsRepo.updateOrderDetails(bookId, orderId, body.getDiscount(), body.getQuantity(), 
				body.getTax());
		
		result.put("Message", "Order Details Updated Successfully");
		result.put("Data", body);
		return result;
	}
	
	// DELETE
	@DeleteMapping("/delete")
	public Map<String, Object> deleteOrderDetails(@RequestParam(name = "orderId") Long orderId,
			@RequestParam(name = "bookId") Long bookId) {
		Map<String, Object> result = new HashMap<String, Object>();
		OrderDetails orderDetailsEntity = orderDetailsRepo.findOrderDetails(orderId, bookId);
		OrderDetailsDto orderDetailsDto = new OrderDetailsDto();
		
		orderDetailsDto = modelMapper.map(orderDetailsEntity, OrderDetailsDto.class);
		
		orderDetailsRepo.deleteOrderDetails(orderId, bookId);
		
		result.put("Message", "Order Details Deleted Successfully");
		result.put("Data", orderDetailsDto);
		return result;
	}
}
