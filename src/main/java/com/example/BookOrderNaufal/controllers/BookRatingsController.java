package com.example.BookOrderNaufal.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.repositories.BookRatingsRepository;
import com.example.BookOrderNaufal.exception.ResourceNotFoundException;
import com.example.BookOrderNaufal.dtos.BookRatingsDto;
import com.example.BookOrderNaufal.models.BookRatings;

@RestController
@RequestMapping("/api/ratings")
public class BookRatingsController {
	ModelMapper modelMapper = new ModelMapper();
	@Autowired
	BookRatingsRepository bookRatingsRepo;
	
	// CREATE
	@PostMapping("/create")
	public Map<String, Object> createRatings(@Valid @RequestBody BookRatingsDto body) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		BookRatings bookRatingsEntity = modelMapper.map(body, BookRatings.class);
		
		bookRatingsRepo.save(bookRatingsEntity);
		
		body.setBookRatingId(bookRatingsEntity.getBookRatingId());
		result.put("Message", "Customer Created Successfully");
		result.put("Data", body);
		return result;
	}
	
	// GET ALL
	@GetMapping("/readAll")
	public Map<String, Object> getAllCustomers() {
		Map<String, Object> result = new HashMap<String, Object>();
		
		List<BookRatings> listRatingsEntity = bookRatingsRepo.findAll();
		List<BookRatingsDto> listRatingsDto = new ArrayList<BookRatingsDto>();
		
		for (BookRatings bookRatingsEntity : listRatingsEntity) {
			BookRatingsDto bookRatingsDto = modelMapper.map(bookRatingsEntity, BookRatingsDto.class);
			listRatingsDto.add(bookRatingsDto);
		}
		
		result.put("Message", "All Ratings Read Successfully");
		result.put("Data", listRatingsDto);
		result.put("Total", listRatingsDto.size());
		
		return result;
	}
	
	// GET SINGLE BY ID
	@GetMapping("/read")
	public Map<String, Object> getRatings(@RequestParam(name = "bookRatingId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		BookRatings bookRatingsEntity = bookRatingsRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("BookRatings", "bookRatingId", id));
		
		BookRatingsDto bookRatingsDto = modelMapper.map(bookRatingsEntity, BookRatingsDto.class);
		
		result.put("Message", "Customer Read Successfully");
		result.put("Data", bookRatingsDto);
		return result;
	}
	
	// UPDATE
	@PutMapping("/update")
	public Map<String, Object> updateRatings(@Valid @RequestBody BookRatingsDto body, @RequestParam(name = "bookRatingId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		BookRatings bookRatingsEntity = bookRatingsRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("BookRatings", "bookRatingId", id));
		bookRatingsEntity = modelMapper.map(body, BookRatings.class);
		bookRatingsEntity.setBookRatingId(id);
		
		bookRatingsRepo.save(bookRatingsEntity);
		
		body.setBookRatingId(bookRatingsEntity.getBookRatingId());
		result.put("Message", "Book Rating Updated Successfully");
		result.put("Data", body);
		return result;
	}
	
	// DELETE
	@DeleteMapping("/delete")
	public Map<String, Object> deleteBookRatings(@RequestParam(name = "bookRatingId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		BookRatings bookRatingsEntity = bookRatingsRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("BookRatings", "bookRatingId", id));
		
		BookRatingsDto bookRatingsDto = modelMapper.map(bookRatingsEntity, BookRatingsDto.class);
		
		bookRatingsRepo.deleteById(id);
		
		result.put("Message", "Book Rating Deleted Successfully");
		result.put("Data Deleted", bookRatingsDto);
		return result;
	}
}
