package com.example.BookOrderNaufal.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.dtos.PaperDto;
import com.example.BookOrderNaufal.models.Paper;
import com.example.BookOrderNaufal.repositories.PaperRepository;

@RestController
@RequestMapping("/api/paperquery")
public class PaperQueryController {

	@Autowired
	PaperRepository paperRepo;
	
	ModelMapper modelMapper = new ModelMapper();
	
	// CREATE
	@PostMapping("/create")
	public Map<String, Object> createPaper(@Valid @RequestBody PaperDto body){
		Map<String, Object> result = new HashMap<String, Object>();
		
		paperRepo.createPaper(body.getQualityName(), body.getPaperPrice());
		
		result.put("Message", "Paper Created Successfully.");
		result.put("Data", body);
		return result;
	}
	
	// READ ALL
	@GetMapping("readAll")
	public Map<String, Object> getAllPaper() {
		Map<String, Object> result = new HashMap<String, Object>();
		List<Paper> listPaperEntity = paperRepo.findAllPaper();
		List<PaperDto> listPaperDto = new ArrayList<PaperDto>();
		
		for(Paper paperEntity : listPaperEntity) {
			PaperDto paperDto = new PaperDto();
			paperDto = modelMapper.map(paperEntity, PaperDto.class);
			listPaperDto.add(paperDto);
		}
		
		result.put("Message", "All Paper Read Successfully");
		result.put("Total Data", listPaperDto.size());
		result.put("Data", listPaperDto);
		return result;
	}
	
	// READ SINGLE
	@GetMapping("/read")
	public Map<String, Object> getPaper(@RequestParam(name = "paperId") Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		Paper paperEntity = paperRepo.findPaper(id);	
		PaperDto paperDto =  new PaperDto();
		
		paperDto = modelMapper.map(paperEntity, PaperDto.class);
		
		result.put("Message", "Paper Read Successfully.");
		result.put("Data", paperDto);
		return result;
	}

	// UPDATE
	@PutMapping("/update")
	public Map<String, Object> updatePaper(@Valid @RequestBody PaperDto body , @RequestParam(name = "paperId") Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		
		paperRepo.updatePaper(id, body.getQualityName(), body.getPaperPrice());
		
		result.put("Message", "Paper Updated Successfully.");
		result.put("Data", body);
		return result;
	}
	
	// DELETE
	@DeleteMapping("/delete")
	public Map<String, Object> deletePaper(@RequestParam(name = "paperId") Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		Paper paperEntity = paperRepo.findPaper(id);
		PaperDto paperDto =  new PaperDto();
		
		paperDto = modelMapper.map(paperEntity, PaperDto.class);
		
		paperRepo.deletePaper(id);
		
		result.put("Message", "Paper Deleted Successfully.");
		result.put("Data", paperDto);
		return result;
	}
}
