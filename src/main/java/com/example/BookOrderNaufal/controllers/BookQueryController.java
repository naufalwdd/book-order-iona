package com.example.BookOrderNaufal.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.dtos.BookDto;
import com.example.BookOrderNaufal.models.Book;
import com.example.BookOrderNaufal.repositories.BookRepository;

@RestController
@RequestMapping("/api/bookquery")
public class BookQueryController {

	@Autowired
	BookRepository bookRepo;
	
	ModelMapper modelMapper = new ModelMapper();
	
	// CREATE
	@PostMapping("/create")
	public Map<String, Object> createBook(@Valid @RequestBody BookDto body) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		bookRepo.createBook(body.getPrice(), body.getReleaseDate(), 
				body.getTitle(), body.getAuthor().getAuthorId(), body.getPublisher().getPublisherId());
		
		result.put("Message", "Book Created Successfully.");
		result.put("Data", body);
		return result;
	}
	
	// READ ALL
	@GetMapping("/readAll")
	public Map<String, Object> getAllBook() {
		Map<String, Object> result = new HashMap<String, Object>();
		List<Book> listBookEntity = bookRepo.findAllBook();
		List<BookDto> listBookDto = new ArrayList<BookDto>();
		
		for (Book bookEntity : listBookEntity) {
			BookDto bookDto = new BookDto();
			bookDto = modelMapper.map(bookEntity, BookDto.class);
			listBookDto.add(bookDto);
		}
		
		result.put("Message", "All Books Read Successfully.");
		result.put("Total Data", listBookDto.size());
		result.put("Data", listBookDto);
		return result;
	}
	
	// READ SINGLE
	@GetMapping("/read")
	public Map<String, Object> getBook(@RequestParam(name = "bookId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		Book bookEntity = bookRepo.findBook(id);
		BookDto bookDto = new BookDto();
		
		bookDto = modelMapper.map(bookEntity, BookDto.class);
		
		result.put("Message", "Book Read Successfully");
		result.put("Data", bookDto);
		return result;
	}
	
	// UPDATE
	@PutMapping("/update")
	public Map<String, Object> updateBook(@Valid @RequestBody BookDto body, @RequestParam(name = "bookId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();

		bookRepo.updateBook(id, body.getPrice(), body.getReleaseDate(), body.getTitle(),
				body.getAuthor().getAuthorId(), body.getPublisher().getPublisherId());
		
		result.put("Message", "Book Updated Successfully");
		result.put("Data", body);
		return result;
	}
	
	// DELETE
	@DeleteMapping("/delete")
	public Map<String, Object> deleteBook(@RequestParam(name = "bookId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		Book bookEntity = bookRepo.findBook(id);
		BookDto bookDto = new BookDto();
		
		bookDto = modelMapper.map(bookEntity, BookDto.class);
		
		bookRepo.deleteBook(id);
		
		result.put("Message", "Book Deleted Successfully");
		result.put("Data", bookDto);
		return result;
	}
}
