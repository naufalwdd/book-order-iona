package com.example.BookOrderNaufal.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.repositories.AuthorRepository;
import com.example.BookOrderNaufal.exception.ResourceNotFoundException;
import com.example.BookOrderNaufal.dtos.AuthorDto;
import com.example.BookOrderNaufal.models.Author;

@RestController
@RequestMapping("/api/author")
public class AuthorController {
	ModelMapper modelMapper = new ModelMapper();
	@Autowired
	AuthorRepository authorRepo;
	
	// CREATE
	@PostMapping("/create")
	public Map<String, Object> createAuthor(@Valid @RequestBody AuthorDto body) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Author authorEntity = modelMapper.map(body, Author.class);
		
		authorRepo.save(authorEntity);
		
		body.setAuthorId(authorEntity.getAuthorId());
		result.put("Message", "Author Created Successfully");
		result.put("Data", body);
		return result;
	}
	
	// GET ALL
	@GetMapping("/readAll")
	public Map<String, Object> getAllAuthor() {
		Map<String, Object> result = new HashMap<String, Object>();
		
		List<Author> listAuthorEntity = authorRepo.findAll();
		List<AuthorDto> listAuthorDto = new ArrayList<AuthorDto>();
		
		for (Author authorEntity : listAuthorEntity) {
			AuthorDto authorDto = modelMapper.map(authorEntity, AuthorDto.class);
			listAuthorDto.add(authorDto);
		}
		
		result.put("Message", "All Genre Read Successfully");
		result.put("Data", listAuthorDto);
		result.put("Total", listAuthorDto.size());
		
		return result;
	}
	
	// GET SINGLE BY ID
	@GetMapping("/read")
	public Map<String, Object> getAuthor(@RequestParam(name = "authorId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Author authorEntity = authorRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Author", "authorId", id));
		
		AuthorDto authorDto = modelMapper.map(authorEntity, AuthorDto.class);
		
		result.put("Message", "Genre Read Successfully");
		result.put("Data", authorDto);
		return result;
	}
	
	// UPDATE
	@PutMapping("/update")
	public Map<String, Object> updateAuthor(@Valid @RequestBody AuthorDto body, @RequestParam(name = "authorId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Author authorEntity = authorRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Author", "authorId", id));
		authorEntity = modelMapper.map(body, Author.class);
		authorEntity.setAuthorId(id);
		
		authorRepo.save(authorEntity);
		
		body.setAuthorId(authorEntity.getAuthorId());
		result.put("Message", "Author Updated Successfully");
		result.put("Data", body);
		return result;
	}
	
	// DELETE
	@DeleteMapping("/delete")
	public Map<String, Object> deleteAuthor(@RequestParam(name = "authorId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Author authorEntity = authorRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Author", "authorId", id));
		
		AuthorDto authorDto = modelMapper.map(authorEntity, AuthorDto.class);
		
		authorRepo.deleteById(id);
		
		result.put("Message", "Genre Deleted Successfully");
		result.put("Data Deleted", authorDto);
		return result;
	}
}
