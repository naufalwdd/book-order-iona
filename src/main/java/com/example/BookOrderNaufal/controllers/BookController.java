package com.example.BookOrderNaufal.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.repositories.BookRepository;
import com.example.BookOrderNaufal.repositories.PublisherRepository;
import com.example.BookOrderNaufal.models.Publisher;
import com.example.BookOrderNaufal.exception.ResourceNotFoundException;
import com.example.BookOrderNaufal.interfaces.BookPriceRate;
import com.example.BookOrderNaufal.dtos.BookDto;
import com.example.BookOrderNaufal.models.Book;

@RestController
@RequestMapping("/api/book")
public class BookController implements BookPriceRate {
	ModelMapper modelMapper = new ModelMapper();
	@Autowired
	BookRepository bookRepo;
	@Autowired
	PublisherRepository publisherRepo;
	
	// CREATE
	@PostMapping("/create")
	public Map<String, Object> createBook(@Valid @RequestBody BookDto body) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Publisher publisherEntity = publisherRepo.findById(body.getPublisher().getPublisherId()).get();
		
		BigDecimal bookPrice = calculateBookPrice(publisherEntity);
		body.setPrice(bookPrice);
		Book bookEntity = modelMapper.map(body, Book.class);
		
		bookRepo.save(bookEntity);

		body.setBookId(bookEntity.getBookId());
		result.put("Message", "Book Created Successfully");
		result.put("Data", body);
		return result;
	}
	
	// CALCULATE BOOK PRICE
	private BigDecimal calculateBookPrice(Publisher publisherEntity) {
		BigDecimal paperPrice = publisherEntity.getPaper().getPaperPrice();
		BigDecimal bookPrice = paperPrice.multiply(bookPriceRate);
		return bookPrice;
	}
	
	// CALCULATE ALL BOOK PRICE
	@PostMapping("/bookPrice")
	public Map<String, Object> calculateAllBookPrice() {
		Map<String, Object> result = new HashMap<String,Object>();
			
		List<Book> listBook = bookRepo.findAll();
			
		for (Book bookEntity : listBook) {
			bookEntity.setPrice(calculateBookPrice(bookEntity.getPublisher()));
			bookRepo.save(bookEntity);
		}
		result.put("Message", "All Books Price Has Been Calculated Successfully");
		return result;
	}	
	
	// GET ALL
	@GetMapping("/readAll")
	public Map<String, Object> getAllBook() {
		Map<String, Object> result = new HashMap<String, Object>();
		
		List<Book> listBookEntity = bookRepo.findAll();
		List<BookDto> listBookDto = new ArrayList<BookDto>();
		
		for (Book bookEntity : listBookEntity) {
			BookDto bookDto = modelMapper.map(bookEntity, BookDto.class);
			listBookDto.add(bookDto);
		}
		
		result.put("Message", "All Book Read Successfully");
		result.put("Data", listBookDto);
		result.put("Total", listBookDto.size());
		
		return result;
	}
	
	// GET SINGLE BY ID
	@GetMapping("/read")
	public Map<String, Object> getBook(@RequestParam(name = "bookId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Book bookEntity = bookRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Book", "bookId", id));
		
		BookDto bookDto = modelMapper.map(bookEntity, BookDto.class);
		
		result.put("Message", "Book Read Successfully");
		result.put("Data", bookDto);
		return result;
	}
	
	// UPDATE
	@PutMapping("/update")
	public Map<String, Object> updateBook(@Valid @RequestBody BookDto body, @RequestParam(name = "bookId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Book bookEntity = bookRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Book", "bookId", id));
		bookEntity = modelMapper.map(body, Book.class);
		bookEntity.setBookId(id);
		
		bookRepo.save(bookEntity);
		
		body.setBookId(bookEntity.getBookId());
		result.put("Message", "Book Updated Successfully");
		result.put("Data", body);
		return result;
	}
	
	// DELETE
	@DeleteMapping("/delete")
	public Map<String, Object> deleteBook(@RequestParam(name = "bookId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Book bookEntity = bookRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Book", "bookId", id));
		
		BookDto bookDto = modelMapper.map(bookEntity, BookDto.class);
		
		bookRepo.deleteById(id);
		
		result.put("Message", "Book Deleted Successfully");
		result.put("Data Deleted", bookDto);
		return result;
	}
}
