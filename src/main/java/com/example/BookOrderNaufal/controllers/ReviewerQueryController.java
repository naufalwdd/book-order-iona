package com.example.BookOrderNaufal.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.dtos.ReviewerDto;
import com.example.BookOrderNaufal.models.Reviewer;
import com.example.BookOrderNaufal.repositories.ReviewerRepository;

@RestController
@RequestMapping("/api/reviewerquery")
public class ReviewerQueryController {

	@Autowired
	ReviewerRepository reviewerRepo;
	
	ModelMapper modelMapper = new ModelMapper();
	
	// CREATE
	@PostMapping("/create")
	public Map<String, Object> createReviewer(@Valid @RequestBody ReviewerDto body) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		reviewerRepo.createReviewer(body.getCountry(), body.getReviewerName(), body.isVerified());
		result.put("Message", "Reviewer Created Successfully.");
		result.put("Data", body);
		return result;
	}
	
	// READ ALL
	@GetMapping("/readAll")
	public Map<String, Object> getAllReviewer() {
		Map<String, Object> result = new HashMap<String, Object>();
		List<Reviewer> listReviewerEntity = reviewerRepo.findAllReviewer();
		List<ReviewerDto> listReviewerDto = new ArrayList<ReviewerDto>();
		
		for(Reviewer reviewerEntity : listReviewerEntity) {
			ReviewerDto reviewerDto = new ReviewerDto();
			reviewerDto = modelMapper.map(reviewerEntity, ReviewerDto.class);
			listReviewerDto.add(reviewerDto);
		}
		
		result.put("Message", "All Reviewers Read Successfully");
		result.put("Total Data", listReviewerDto.size());
		result.put("Data", listReviewerDto);
		return result;
	}
	
	// READ SINGLE
	@GetMapping("/read")
	public Map<String, Object> getReviewer(@RequestParam(name = "reviewerId") Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		Reviewer reviewerEntity = reviewerRepo.findReviewer(id);
		ReviewerDto reviewerDto =  new ReviewerDto();
		
		reviewerDto = modelMapper.map(reviewerEntity, ReviewerDto.class);
		
		result.put("Message", "Reviewer Read Successfully.");
		result.put("Data", reviewerDto);
		return result;
	}
	
	// UPDATE
	@PutMapping("/update")
	public Map<String, Object> updateReviewer(@Valid @RequestBody ReviewerDto body , @RequestParam(name = "reviewerId") Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		
		reviewerRepo.updateReviewer(id, body.getCountry(), body.getReviewerName(), body.isVerified());
		
		result.put("Message", "Reviewer Updated Successfully.");
		result.put("Data", body);
		return result;
	}
	
	// DELETE
	@DeleteMapping("/delete")
	public Map<String, Object> deleteReviewer(@RequestParam(name = "reviewerId") Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		Reviewer reviewerEntity = reviewerRepo.findReviewer(id);
		ReviewerDto reviewerDto =  new ReviewerDto();
		
		reviewerDto = modelMapper.map(reviewerEntity, ReviewerDto.class);
		
		reviewerRepo.deleteReviewer(id);
		
		result.put("Message", "Paper Deleted Successfully.");
		result.put("Data", reviewerDto);
		return result;
	}
}
