package com.example.BookOrderNaufal.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.repositories.ReviewerRepository;
import com.example.BookOrderNaufal.exception.ResourceNotFoundException;
import com.example.BookOrderNaufal.dtos.ReviewerDto;
import com.example.BookOrderNaufal.models.Reviewer;

@RestController
@RequestMapping("/api/reviewer")
public class ReviewerController {
	ModelMapper modelMapper = new ModelMapper();
	@Autowired
	ReviewerRepository reviewerRepo;
	
	// CREATE
	@PostMapping("/create")
	public Map<String, Object> createReviewer(@Valid @RequestBody ReviewerDto body) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Reviewer reviewerEntity = modelMapper.map(body, Reviewer.class);
		
		reviewerRepo.save(reviewerEntity);
		
		body.setReviewerId(reviewerEntity.getReviewerId());
		result.put("Message", "Reviewer Created Successfully");
		result.put("Data", body);
		return result;
	}
	
	// GET ALL
	@GetMapping("/readAll")
	public Map<String, Object> getAllReviewer() {
		Map<String, Object> result = new HashMap<String, Object>();
		
		List<Reviewer> listReviewerEntity = reviewerRepo.findAll();
		List<ReviewerDto> listReviewerDto = new ArrayList<ReviewerDto>();
		
		for (Reviewer reviewerEntity : listReviewerEntity) {
			ReviewerDto reviewerDto = modelMapper.map(reviewerEntity, ReviewerDto.class);
			listReviewerDto.add(reviewerDto);
		}
		
		result.put("Message", "All Reviewer Read Successfully");
		result.put("Data", listReviewerDto);
		result.put("Total", listReviewerDto.size());
		
		return result;
	}
	
	// GET SINGLE BY ID
	@GetMapping("/read")
	public Map<String, Object> getReviewer(@RequestParam(name = "reviewerId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Reviewer reviewerEntity = reviewerRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Reviewer", "reviewerId", id));
		
		ReviewerDto reviewerDto = modelMapper.map(reviewerEntity, ReviewerDto.class);
		
		result.put("Message", "Reviewer Read Successfully");
		result.put("Data", reviewerDto);
		return result;
	}
	
	// UPDATE
	@PutMapping("/update")
	public Map<String, Object> updateReviewer(@Valid @RequestBody ReviewerDto body, @RequestParam(name = "reviewerId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Reviewer reviewerEntity = reviewerRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Reviewer", "reviewerId", id));
		reviewerEntity = modelMapper.map(body, Reviewer.class);
		reviewerEntity.setReviewerId(id);
		
		reviewerRepo.save(reviewerEntity);
		
		body.setReviewerId(reviewerEntity.getReviewerId());
		result.put("Message", "Reviewer Updated Successfully");
		result.put("Data", body);
		return result;
	}
	
	// DELETE
	@DeleteMapping("/delete")
	public Map<String, Object> deleteReviewer(@RequestParam(name = "reviewerId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Reviewer reviewerEntity = reviewerRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Reviewer", "reviewerId", id));
		
		ReviewerDto reviewerDto = modelMapper.map(reviewerEntity, ReviewerDto.class);
		
		reviewerRepo.deleteById(id);
		
		result.put("Message", "Reviewer Deleted Successfully");
		result.put("Data Deleted", reviewerDto);
		return result;
	}
}
