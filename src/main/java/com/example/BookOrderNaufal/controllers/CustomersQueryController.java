package com.example.BookOrderNaufal.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderNaufal.dtos.CustomersDto;
import com.example.BookOrderNaufal.models.Customers;
import com.example.BookOrderNaufal.repositories.CustomersRepository;

@RestController
@RequestMapping("/api/customersquery")
public class CustomersQueryController {

	@Autowired
	CustomersRepository customersRepo;
	
	ModelMapper modelMapper = new ModelMapper();
	
	// CREATE
	@PostMapping("/create")
	public Map<String, Object> createCustomers(@Valid @RequestBody CustomersDto body) {
		Map<String, Object> result = new HashMap<String, Object>();
		customersRepo.createCustomers(body.getAddress(), body.getCountry(), body.getCustomerName(), 
				body.getEmail(), body.getPhoneNumber(), body.getPostalCode());
		result.put("Message", "Customers Created Successfully.");
		result.put("Data", body);
		return result;
	}
	
	// READ ALL
	@GetMapping("/readAll")
	public Map<String, Object> getAllCustomers() {
		Map<String, Object> result = new HashMap<String, Object>();
		List<Customers> listCustomersEntity = customersRepo.findAllCustomers();
		List<CustomersDto> listCustomersDto = new ArrayList<CustomersDto>();
		
		for(Customers customersEntity : listCustomersEntity) {
			CustomersDto customersDto = new CustomersDto();
			customersDto = modelMapper.map(customersEntity, CustomersDto.class);
			listCustomersDto.add(customersDto);
		}
		result.put("Message", "All Customers Read Successfully");
		result.put("Total Data", listCustomersDto.size());
		result.put("Data", listCustomersDto);
		return result;
	}
	
	// READ SINGLE
	@GetMapping("/read")
	public Map<String, Object> getCustomer(@RequestParam(name = "customerId") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		Customers customersEntity = customersRepo.findCustomer(id);
		CustomersDto customersDto = new CustomersDto();
		customersDto = modelMapper.map(customersEntity, CustomersDto.class);
		
		result.put("Message", "Customer Read Successfully.");
		result.put("Data", customersDto);
		return result;
	}
	
	// UPDATE
	@PutMapping("/update")
	public Map<String, Object> updateCustomers(@Valid @RequestBody CustomersDto body, 
			@RequestParam(name = "customerId") Long customerId) {
		Map<String, Object> result = new HashMap<String, Object>();
		customersRepo.updateCustomers(customerId, body.getAddress(), body.getCountry(), 
				body.getCustomerName(), body.getEmail(), body.getPhoneNumber(), body.getPostalCode());
		
		result.put("Message", "Customer Updated Successfully.");
		result.put("Data", body);
		return result;
	}
	
	// DELETE
	@DeleteMapping("/delete")
	public Map<String, Object> deleteCustomer(@RequestParam(name = "customerId") Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		Customers customersEntity = customersRepo.findCustomer(id);
		CustomersDto customersDto =  new CustomersDto();
		
		customersDto = modelMapper.map(customersEntity, CustomersDto.class);
		
		customersRepo.deleteCustomers(id);
		
		result.put("Message", "Customer Deleted Successfully.");
		result.put("Data", customersDto);
		return result;
	}
}
